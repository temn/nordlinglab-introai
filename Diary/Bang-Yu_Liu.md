This diary file is written by Bang-Yu Liu N66084616 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-19 #

* The article mentioned that ”what seems easy is actually hard…”, I agree with that. I know a little ML theory about text recognition. So I know that even the single number recognition is also complicated.

* “Suitcase words” : One word with so many meanings.

* DL is a subfield of ML, ML is a subfield of AI.

* “Robot” : Feel something from us and do something to us.


# Article summary #

* Journal paper: Safety leading indicators for construction sites: A machine learning approach

* We cannot predict which algorithm is the best in the specific study, we can only pick more than one algorithms and try.

* Training a prediction model requires a large amount of data. It is difficult to obtain relevant data unless the company is willing to cooperate.

* From this study, we can know that the result of accident may be due to a reason we never thought of, and we can use ML to find out these causes.

* Even if we find out that some features do affect the prediction results, they may not be explained in the real world.


# 2020-03-26 #

* I've watch this: https://www.youtube.com/watch?v=aircAruvnKk

* I learn about the basic concept of machine learning.

* Multilayer Perceptron(MLP): A old but basic knowledge for ML.

* Bias: Express how weighted sum is meaningful.

* Cost: Should be small.


# 2020-04-02 #

* Supervised Learning: For example, after inputting a large number of bicycles and motorcycles with marked labels to the machine, let the machine distinguish whether the photos without labels are motorcycles or bicycles.

* Unsupervised Learning: This machine learning method does not require human input labels beforehand. It only provides input examples. During learning, the machine automatically finds the rules of potential categories, iteratively tests the results of the study and applied to new cases.


# 2020-04-09 #

* Data is the cornerstone of machine learning. Without good data, it is difficult to generate good data value(Result).

* Prerequisites for machine learning: Data Preparation.

* Data Preparation: Data ETL(Extract, Transfer, Load), Data Cleansing, Data Mining.


# 2020-04-16 #

* The convolutional neural network consists of a convolutional layer, a pooling layer, and a fully connected layer.

* Convolutional Layer: When the convolutional neural network wants to recognize a picture, it does not know where the image features are at the beginning. The convolutional neural network will compare every place in the image.

* Pooling Layer: Pooling can downsample the images. Downsampling can reduce the number of parameters and the amount of calculation, reduce the number of data points to be processed and reduce the time required for calculation.

* Fully Connected Layer: After extracting the image feature values through the convolutional layer and the pooling layer, the fully connected layer functions as a classifier in the convolutional neural network.


# 2020-04-23 #

* CNN process the image with every small pixel area maintaining the continuity among the features.

* The model needs a large amount of data for training. 

* Different CNN model structure can lead to distinct prediction result.

* Tools: TensorFlow is an open source library developed by Google. Keras is TensorFlow's high-level API for building and training deep learning models, written in Python and capable of running on top of TensorFlow.


# 2020-04-30 #

* Overfitting means over-study the training data, and it becomes impossible to predict or distinguish other data that is not in the training data.

* Now that the training data is getting bigger and bigger and the training time is getting longer and longer, avoiding and solving Overfitting is an important issue in machine learning.

* Reasons for Overfitting: Too few training data, too many parameters, too powerful model.


# 2020-05-07 #

* In the mathematical formula, we can partition the mean squared error loss and decompose two different terms: the first term is called the bias error, and the second term is called the variance error.

* The high bias error is usually caused by the insufficient capacity of the model and the failure to describe the average distribution of the training data correctly. At this time, we can call the model as underfitting.

* In the case of underfitting, adding training examples does not improve the performance of the training model. Only by increasing the complexity of the model can better performance be obtained.

