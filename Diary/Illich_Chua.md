This diary file is written by Illich Chua I84055018 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-03 # Welcome 

* Welcome video was posted, thanks to all the effort of the Nordling Team (it must have been hard)
* Excited to learn about a whole field that I currently have no idea about

# 2020-03-21 # Success Story HW

* Troubled on what topic I should do my HW on
* It was hard to not repeat a topic as the other students...
* Discovered a lot of application of AI in a variety of unthinkable fields: ocean, pollution, traffic, cooking, etc.
* Most memorable one that I saw and wanted to center my HW on was about cooking (robotic kitchen)
* Decided against it as they were still in the prototype stage
* Another reason for which is because there was a crucial feature in which they currently do not have.
* Moley and Spyce were the two very interesting products 
* Spyce is already operating in the United States, however can only process orders -> retrieve prepared ingredients -> mix  (https://www.youtube.com/watch?v=rfMZfxgbuCw)
* Moley is still a prototype that aims to mimic a real chef's actions in cooking and has access to thousands of recipes on a database
* However, the feature that i was looking forward to was hoping these robotic kitchens can perform early preparation of the ingredients too (washing, cutting, sorting) 
* Another applciation I am currently looking forward to (under development) are AI-assisted detection of drivers using their phones and drunk driving.

# 2020-03-29 # Introduction to AI - Ch 1

* Not "an AI" but "an AI method" -> AI is not a countable noun!
* Personally, in addition to the vague definition of AI, the whole concept seems a little too hard to grasp for me. "How can they make human-like actions and computations into a string of equations?" "How do machines LEARN???"
* Intelligence is not only composed of a single dimension! Howard Garner's 9 intelligences are as follows:
	** Naturalist (nature smart)
	** Musical (sound smart)
	** Logical-mathematical (number/reasoning smart)
	** Existential (life smart)
	** Interpersonal (people smart)
	** Bodily-kinesthetic (body smart)
	** Linguistic (word smart)
	** Intra-personal (self smart)
	** Spatial (picture smart)
	** I think that most people think of AI as the logical-mathematical type, perhaps because thats usually what we develop them to be, since it's much more of an effort for us humans to perform?
	** Another interesting thing about AI anticipate the unknown or learn to based from past experiences? Such was stated in the article, "when someone else opens a door just as you are reaching for the handle, and then you can find yourself seriously out of balance." 
		Although I have seen an application of AI method to be an acrobat that tumbles and jumps, I have to wonder, will the AI know how to balance itself or react accordingly to how a human would in unexpected circumstances?
		Perhaps the way our body's nerves and reflexes are still a much deeper and unknown field for AI learning, but i believe it will be possible.
* Reading the conversation with Eugene Goostman was very interesting. It was making subtle jokes and I can't help but wonder again how is a personality encoded??
* Interested to know MORE about how AI is programmed, but perhaps through a diagram, flowchart, or something more visual to represent the whole thing would be more effective than in words :-)

# 2020-04-30 # Introduction to Python (Getting help and comments, Printing, Conditional statements and loops)

* Wrote an entry a couple of days ago but forgot to commit.... 
* I was hoping there would be perhaps an introductory lecture on the basics of programming, and not immediately jumping to learning how to code.. It will help the students grasp the concept better and spark more interest in learning.
* Went to <https://www.freecodecamp.org/news/a-gentler-introduction-to-programming-1f57383a1b2c/> to learn about the most basic things from programming and it mentioned how there are two shells: CLI and GUI. I think i have only ever used the CLI cmd to check for my ping when i used to play online games.. haha
* Today I learned the basics. It is quite interesting and challenging since I have to look at the examples and type. Perhaps with more practice the codes can come out more naturally just like how I'm typing this without even making much effort! haha
* Created my own Google Colab file to practice :D 
* On the "Printing" example, << print("Here", type(y), "was converted to", type(str(y)))>> Did not work. But as i removed the "TYPE" command, it worked. I searched for the meaning of type but it still doesn't make any sense to me..
* Have to remind myself to put the comments (#) so that I won't forget what I'm doing when I visit my file after a long time!
* Went on Youtube [The Net Ninja] to learn the basics about Python. I learned the code "type," numbers, strings, if statements, lists, etc.*
* Learned that there are "expressions" and "actions" and somehow got the ghist of how to differentiate them. An example of action would be .append while for expression .upper. Printing the whole statement for expression is necessary, while only prinitng the list name or modified object in action is necessary
* Successfully did the exercises on my own! It was rough at first but I feel very proud!

# 2020-06-7 # Introduction to Python (Variables and functions, Reading and writing files, Error handling)
* Catching up on past homeworks!
* I learned how to make an interactive mode while learning about functions! It was very interesting and pleasing
* Also it was interesting to see how areas of circles are computed!
* I think I need more practice for the coding to come naturally for me such as typing is!

# 2020-06-17 # Introduction to Neural Networks and Modelling
* Finally learning about neural networks!!
* I think it's really intriguing and hard to grasp how machines can learn and teach themselves!!
* The video provided is pretty helpful! I felt like it was reading my mind with all those questions he addressed but I think some terms may still be confusing to someone without linear algebra concepts (or someone in the medical background like me), so i went to look for "neural network for dummies" videos :D
	** I think this alternate 5 minute video is a good supplement to the initial one posted <https://www.youtube.com/watch?v=bfmFfD2RIcg>, it gives a good overall simple concept suitable for beginners (or dummies like me)
	** this 5 minute video also has a simple review quiz at the end! 
	** Machines learn via continuous forward and backward propagation (similar to physiological feedbacks?)
	** Backward: the correct answer is compared to the output and functions within the layers are adjusted until it has high probability or low error!
* Interesting applied examples: Forecasting (weather, stocks), Facial recognition (filters on snapchat too, i guess!), music composition
* Will be continue watching his next video (and the reading assingment) for another day :) Linear algebra is ....

# 2020-06-24 # Ethics and the danger of algorithm bias 
* The biases and blind spots of the present time's is further amplified by the AI. It has beeng going on since more than a hundred years ago, and it's hard to eradicate. Now, the AI can do much more than what we can do, what if we were to utilize AI to eradicate these biases and work towards a better world?
* Famous scientists says that AI will end the world, AI is an existential risk
* Algorithm bias may just be something that people usually overlook, however, if it deals with your civil rights, freedom, and life- that's where it poses as a problem that needs to be solved.
* Algorithmic bias doesnt really make fair decisions. Since we are the one feeding them datasets, we can start creating more inclusive training sets, full spectrum.
* A world where technology works for us, not only some of us. (Seems like the exclusion nature of human has only been amplified with AI)

# 2020-06-25 # Midterm Project
* Initially discussed and worked with my team (3 people total) to work on the midterm project. 
* I think it was very interesting to see the neural networks in action when the machine is TEACHING itself and predicting the answers.
* For our project, we initially had audio problems so we had to reupload the youtube video.
* I also find the project of Group 2 (THE H.U.G.O.) <https://www.youtube.com/watch?v=p4UrZd69kA4&feature=youtu.be> to be very interesting! They had created what i initially had in mind: where they can write their own "test data" and let the machine predict it, instead of letting the machine do a random generation of "test data"!
* I saw that their model could predict what number or letter they wrote --> make it into an image --> and then predict
* However, at 4:42, they wrote what seems like a "1" but it was recognized as an L! But their accuracy was only about 85% so overlooking this simple mistake, their model is actually not bad already!!
* 

# 2020-06-28 # Automation and the future of work 
* Tried to learn about programming and AI due to the steadily increasing automation and replacement of human workers
* Can't help but think that programming might just become one of the required languages to survive in the future!
* Even before machines, I think my profession:  Pharmacist, is also a job in danger of being automated
* I think it is important to have a side hustle that is unique to myself that is "unreplaceable" by machines
* Paired with the steady improvement and mass acceptance of the internet, perhaps this is also why many people result to utilizing social media to jumpstart and help stay on their careers as vloggers, video tutorials, etc.
* Actually was surprised to find that USA is more on the renewable energy subsidy more than the fossil-fuel
* [Afterthought from the PDF content] Also recently heard a speech from a classmate on how electric vehicles can be better than the traditional ones, other than being eco-friendly:
* 1. As battery prices drop, the costs for the materials to make the vehicles also substantially drop. 
* 2. Battery/Electric powered vehicles are more efficient than gasoline, thus the cost per mile is much less for the former than for the latter.
* Leads me to agree again with how automation and newer ways introduced can be more efficient than traditional ways (human force) as mentioned in the video.
* Recently saw a funny article (posted a few days ago) <https://www.businessinsider.com/tesla-autopilot-mistakes-burger-king-stop-sign-new-ad-2020-6> wherein the Tesla Autopilot mistook a Burger King sign for a stop sign! and then it proceeded to continue driving after realizing it's not a stop sign.. 
* Speaking of renewable energies, I think a solar panel is really efficient and eco-friendly. Common answers to why people won't install these panels are most probably due to the cost. (Perhaps there could be a better breakdown of the better deal)
* However, one uncommon reason i heard was because the panels ruin the overall aesthetic of the house! But only a few days ago, I came upon a video wherein the solar panels are made curvy (not common flat panels on the roofs) and shaped into the roof tiles! <https://www.youtube.com/watch?v=p4UrZd69kA4&feature=youtu.be> 
* Makes me think how problems are always present but mankind never stops to solve these problems! (although expensive)
* In light of the DNA sequencing, I can say that genetic therapy and immunotherapy is really the future. Cancer drugs are very lethal and even the most selective ones still cause minimal damage to healthy cells, therefore, these immunotherapy/gene therapy is one way to reach the selective killing of cancer cells without harming the remaining healthy cells.
* Im actually surprised (PDF p.62) that Korea had the highest number of industrial robots! Well, their technolog is pretty advanced too anyway so i think it still makes sense!  Whereas I think in China it fits my initial thoughts since they have a massive population..

# 2020-06-29 # Current AI research 
* Watched a snippet of the Tesla Coference Event video. It was very informative and I was very amazed on how they "design" them.
* The car had two independent chips that do their own thing, then they recheck each other. Sensors are also installed to ensure that the proper commands are executed properly i.e. acceleration
* Also very cool how the motherboard is just so small! the electric chips are also very small...

# 2020-07-05 # Introduction to TensorFlow
* The explanation by Martin Görner was good! He seemed to know the questions I have in mind! But there are still some parts where I'm very confused
* The basics of neural networks by 3blue1brown in Youtube(posted in the Introduction to Neural Networks section) and the other additional video I watched are very helpful when listening to Martin Görner! 
* Because they were mostly visualization, and sometimes I had to listen only to Martin without seeing his slides while he's talking.. So it was very helpful to have the images from the prior videos :D
* It is also very interesting to see how overtraining can decrease the accuracy of the model!!
* 