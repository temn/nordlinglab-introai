It should be written in Markdown. Note that you should write the diaries for all weeks in the same file.

This diary file is written by Shemon Baptiste N16087054 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2021-02-25 #

* I missed the first lecture, I had to decided whether or not I needed to take the course since I need 6 credits but this was only 2.
* I think it's worth taking, the world is becoming more and more advanced and more and more jobs require some basic knowledge in programming.

# 2021-03-04 #

* The second lecture I was able to attend, it was interesting.
* I learnt about the similarities and differences between certain terminologies you use with AI and deeplearning.
* It was quite interesting how so many of those ideologies overlap and this point was clearly illustrated using venn diagrams which made understanding easier.
* It was interesting how the definitions from different sources about the same ideology contradicted each other.

# 2021-03-11 #

* Today we had a short introduction on Python, it was a great lesson.
* We learnt how it is important as engineers to test whether or not our code is working and "print()" is the function in python to do so.
* We also learnt how to get help using the help function and Jupyter.
* For the Homework I was able to find a success story, AI in healthcare, specifically for diabetes retinopathy.
* It's a very interesting paper that explains all the aspects and techniques clearly, although there are some parts of it I still don't understand.
* I have already done the ppt for the success story however the last slide, since it contains parts of the article I dont understand I was nervous I would have had to present today.
* It's great that presentations start next week. I now have more time to try to understand my ppt's content.

# 2021-03-18 #

* This week we continued learning more about Python, although I was late, the ppt was clearly written so it was quite easy to understand and catch up.
* I don't have a computer, however I do have a tablet which I previously was able to install tensorflow and the other units on.
* Installing Jupyter on the otherhand is what became a problem, apparently the latest python version available on the tab is 3.6 and Jupyter needs 3.7 according to the error.
* I learnt that coding in command prompt is a bit tricky, and I should use the script to code.
* During class I had to use google colaboratory, however when I was practising on my tablet I found there was no issues doing the basic commands in the exercises.

# 2021-03-25 #

* I couldn't attend class this week, but since the ppt was clearly written so it was quite easy to understand. Hopefully I will be able to catch up.
* This course is also an online course, so I should be able to view the class videos, but I unfortunately couldn't find where they are, I have to remember to ask Jacob.
* I have been extremely busy this week, my lab has qualifying exams coming up so it has been stressful.
* I haven't completed all of the exercises by today, however I plan to finish them before next class so if I don't understand something I can ask questions and I won't be behind.
* It is very helpful that links to help us to understand the content are placed in the ppt.

# 2021-04-01 #

* This week was a holiday, and we didn't have class, I wasn't sure if I was supposed to upload an entry and honestly forgot.
* I was able to catch up to the classwork done in school.
* I'm now comfortable with basic commands in python.

# 2021-04-08 #

* This week we learnt about building a digit recognition webservice with Tensorflow. I thought it wasn't very difficult but I also dont think it's easy.
* The video shown was very interesting and I learnt a bit, like the initial mistake was they simplified the model too much.
* I still think I need more practice, I plan to do that sometime next week.

# 2021-04-15 #

* This week's lesson produced discussion about artificial intelligence by us trying to think of ourselves as though we were the AI. This was brought about by the question what is our objective.
* This was a fruitful discussion because conceptually I think I understand what an objective function is and its use. I had kept hearing the term but never really grasped its concept
* A question came up about allowing AI to have emotions. The point was made that anger and hate are emotions too and if this was wise as it can create unnecessary conflict. Professor suggested to ask the question again when we discuss ethics.
* With regards to my own self study, I have discovered the TensorFlow, Keras and and deeplearning without PhD write file which I have begun to study.
* I hope it will help me to improve the digit code I was working on.
* My other goal is to fully understand that code.

# 2021-04-22 #

* These past couple of weeks have been quite eventful for me. I am finding it harder to find a balance between my schoolwork and personal life.
* Between covid and a volcanic eruption in my home country and trying to complete my project and my classes as well as assignments so that I can graduate, it's becoming quite a handful. 
* This week I missed class, I was told on wednesday evening I needed to send a ppt of my project to the TA and professor for a class presentation by Friday... I was to present on May 5th but due to reasons it was brought forward.
* The good news is that I had already had all my information put together in ppts so I had to spend my Wednesday night and Thursday all day and night putting together the ppt.
* The bad news is I am now a week behind for classes as I wasnt able to do much studying on the PhD write file.
* This week I am a bit disappointed with my progress, I hope I can quickly catchup.

# 2021-04-29 #

* This weeks class was very interesting. We learnt about different loss functions.
* It's interesting how each one has its own application.
* It apparent that knowing your system is vital in knowing when to use the most appropriate loss function.
* We only covered about four in class but reading all peaked my interest in how well the ones with similar application compare in functionality with each other.
* I still havent caught up studying on the PhD write file, but I'm getting there.

# 2021-05-06 #

* I missed this week's class, I have started going through the ppt but, i have honestly been a bit distracted, there are too many things happening.
* I think i have to go through it a bit more, i'm a bit lost.
* In terms of my own practise, it has been going well I was able to cover some more of the PhD write file but i dont think enough.

# 2021-05-20 #

* This weeks class was online. It was very interesting and engaging.
* I am supposed to graduate this semester and I am working on my thesis project, so I didn't get a chance to watch the videos before class.
* We watched the videos in class and they were quite attention grabbing, the following discussions were also quite fruitful.
* I walked away understanding algorithm bias, and how real world social situations affect artificial intelligence with algorithm bias.
* I haven't had a chance as yet but I plan to watch the Coded bias movie, once i finish my ppt's for this week i'll do that.
* Something that i thought was haunting from class was the fake videos of real people. There is so much potential for propoganda and fake news, it's scary.

# 2021-05-27 #

* This week we didn't have class. Instead we were to work on our group project.
* My groupmate contacted me. We discussed our experience with coding, unfortunately he informed me he doesnt have much.
* I dont have much either, but i think that that's ok. We can study hard and help each other.
* He said he didnt have any experience with MNIST, but i have been practising with the information professor gave so I sent him the links that helped me practice.
* I haven't told him as yet but I think ill check on how much he has learnt on monday so we can discuss what to do next.
* From the example we can somewhat tackle the problem, we just need to discuss the best way to do so.

# 2021-06-03 #

* This week's class was interesting. I know I usually say that but, I have a short attention span, and with these classes I usually pay attention from start to finish.
* I unfortunately missed the first 30 minutes of class, I have recently been busy trying to finish my thesis so I get lost in time.
* I didn't get to take part in the Future work vision conversation, but I would like to work Freelance and as an entrepreneur in the engineering field.
* The benefits I expect are basically high pay and more freedom to manage my own hours.
* I found it interesting how most if not all human done jobs are basically replacable.
* In terms of the homework, my groupmate and I are in contact. He tried several models, I had done so as well some weeks ago, but I don't know how to show him my code and results.
* He used notion.no and I was able to see his, next week we have no class but during class I think ill ask him how to use this website and upload my own results for him to see.
* With the models I tried my best was 98%, but sun was able to get 99.3%.
* I need to look through his models and see what he did differently and how I can improve my own.
* I hope to do so before class Thursday next week so i'll have lots of questions for Sam.

# Article Summary #

* Deep-learning is increasingly being recognized as an essential tool for artificial intelligence research, with applications in several areas, however, there has been none in the field of real traffic flow conditions.
* This paper lays the framework for developing the TensorFlow™ DNN architecture to analyse and estimate traffic conditions, using real-time transportation big data. 
* The authors suggested a (Deep Neural Network)DNN model which they aimed to distinguish congested conditions from non-congested conditions through logistics regression analysis. 
* They used Traffic Performance Index (TPI) for logistic regression analysis of the real time data obtained from vehicles with an on-board device, and the hyper tangent function used as an activation function for each layer.
* The suggested model is effective in identifying real traffic flow conditions with an estimated accuracy of 99%, however it has a big issue of limitations to memory capacity and excessive CPU calculation times.
* The video for this summary can be found at the link https://youtu.be/1AI7MOuYb8Y

# 2021-06-17 #

* The previous week was really hectic for me. I almost had a meltdown during class because I hadn't realized I did the wrong homework and it was to be a big part of our grade.
* Thank goodness I keep my camera and microphone off, otherwise everyone would have seen be get nervous and start bawling my eyes out before I pulled myself together and emailed professor about what happened.
* I take my work seriously and it isn't often I make a faux paux like this, I have alot on my plate this semester so I tried doing my assignments before to stay ahead and not get overwhelmed.
* Some good that did me, I ended up overwhelmed anyways. But thank goodness professor understood and gave me 4 days to complete it.
* I'm not sure if to say I work well under pressure or not, you would understand at the end... but I initially found a paper called Co-teaching: Robust training of deep neural networks with extremely noisy labels.
* I read it and liked it, it was quite informative, so I made a ppt about it between friday and saturday, but as I was about to upload on saturday while rereading the instructions I realised I had missed a key word...supervised.
* Facepalm moment! There I was, just spent 2 days reading understanding compiling the information into a 10 page ppt file only to realise though informative it mentioned nothing of supervised learning.
* So I went back to the drawing board and found another paper, Deep supervised learning for hyperspectral data classification through CNN, but at this point, my mind saturated, my time running out and me not knowing what the hell hyperspectral data classification is, started another meltdown.
* After calming down, because melting down helps noone, I started some research on Hyperspectral data classification, I found it quite facinating, its based on spectroscopy which I am familar with, however, maybe it was due to my saturation, I found the data hard to digest.
* As a result, I felt as though I won't be able to do the paper justice, so I found another one, Deep neural networks for traffic flow prediction by Yi2017.
* This paper I found was simple, easy to understand and I would be able to explain and answer questions.
* I found it facinating how they could use real live real time information to predict traffic conditions accurately.
* But as a result I worked throught the weekend and totally forgot to update my diary.
* With regards to the final project, and this weeks work, Sam had exams this week so we hardly had time to discuss together since he was so busy, however, I did some solo work.
* Unfortunately with all that I have tried 98.6% is the best accuracy I got. Some were actually as bad as 30%, I found that adding maxpooling layer to my code made it a bit better but not enough to make a significance.
* On Monday we plan to meet online and discuss our findings and make our final decision.

# 2021-06-24 #

* Today was the last day of class and our test. I didn't do very well for the test. I remembered the discussions, but I wasnt very good at recalling the details,so i was disappointed in my score.
* For my code I ended up using LeNet's architecture idea, and the basic code on tensorflow, along with trial and error from reading tons of Kaggle codes, an accuracy of 99.28%.
* Sam however was able to use his combined CNN architecture to get an accuracy of 99.59%, so we ended up using his code.
* The video presentation can be found at the link https://youtu.be/dXtl8MzNhbM
* I feel proud of myself that I was able to increase the accuracy, I read that increasing it by 1% from 98% is really difficult, so I'm really proud of myself and of Sam who got it to increase by 1.5%.
* As always, Professor Nordling's classes are a treat, especially for someone like me with attention issues. The interactiveness of the class made sure my brain was always focused so i drifted away less.
* This weeks class was very interesting, but there are alot of concepts I still don't quite grasp.
* Last week we had discussed the dropout technique, but I forgot to write about it in my diary.
* I was able to find 3 websites that discussed dropout but only 2 covered the 3 criterias we discussed it should have.
* I feel great about my progress this week, I was able to write the code for the digit recognition.
* I found that by increasing the number of epoch, increases the accuracy, but there is a drawback.
* It has a drawback where the calculation time increases.
* I hope to continue with my progress, this week I plan to try the more advanced code.
* With the beginners code and changing the number of epochs to 10 i was able to go from an accuracy of 97% to 98%.
* I hope with the more advanced code I can increase the accuracy.
* I still havent caught up studying on the PhD write file, but I'm getting there.