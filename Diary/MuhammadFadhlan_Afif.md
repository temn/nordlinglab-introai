This diary file is written by Muhammad Fadhlan Afif N16097017 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-03-19 #

* There are video links in explanation of Lecture material and Assignments section that leads to the same video (I believe it supposed to lead to different video).
* I learn something from https://course.elementsofai.com/1
* I believe that news, information from medias, movies and science-finction maybe lead me to wrong understanding about AI.
* I think the annalogy in between 'AI' and 'Biology' is great. AI is dicipline or collection of concepts, problems, and methods for solving certain task.
* I think that the task, which is more need intuitive aspects, will be harder to be solved or done by AI.
* On the other hand, tasks, which required logics or probabilites or calculations, is more easy to be solved by AI.
* I also learnt that we cannot compare AI systems with different task.
* I hope that learning further about AI may develop my ability to cooperate with people who really focus on it.
* I try to read the article "Design and Implementation of Heuristic Intelligent Attack Algorithm in Robot Soccer System Small Size League"

# Article summary #
* Summary about "Design and Implementation of Heuristic Intelligent Attack Algorithm in Robot Soccer System Small Size League" article:
* Various collaborative attacker strategy successfully implemented
* Various collaborative attacker strategy were evaluated by counting the successful trials in five trials experiment each.
* These collaborative attacker strategy were recommended to be used in certain game situation or scenario
* Optimizing the performance of AI sensing accuracy by vision system towards a better and more precise
* Combining this collaboration strategy with defense collaboration strategy with a combination of a lot more various dynamic games

# 2019-03-25 #

* I start to see the ppt "Success stories where an Artificial Intelligence outperform humans"
* I watched the video about turing test. Some programs have passed the test.
* Found out that conciousness is not about memory size or processing power.
* I am realy surprised with Squirrel AI outpeform human teacher.
* Maybe this kind of AI can assist every person to pursue his/her career at their best
* I try to read the article "LIPNET: END-TO-END SENTENCE-LEVEL LIPREADING"

# Article summary #
* LipNet, the first model to apply deep learning to end-to-end learning of a model that maps sequences of image frames of a speaker�s mouth to entire sentences.
* The end-to-end model eliminates the need to segment videos into words before predicting a sentence.
* LipNet greatly outperforms a human lipreading baseline, exhibiting 4:1 better performance.
* In future work, they hope to demonstrate this by applying LipNet to larger datasets, such as a sentence-level variant of that collected
* They aim to apply this approach to a jointly trained audiovisual speech recognition model, where visual input assists with robustness in noisy environments.

# 2019-04-04 #

* I start to learn about phython programming this week
* I have learn C++ before. This was helping me to learn Phython.
* I do not understand about Jupyter
* Still have to figure it out about Classes and inheritance
* I try to read the article "Artificial cognition for social human-robot interaction: An implementation"

# Article summary #
* The system achieves multi-modal and interactive grounding in complex real environments involving one or several humans and a robot
* The system supports a distributed computation of symbolic knowledge for situated dialogue, thanks to the combination of perspective taking, affordances computation and logical inference
* It provides generic mechanisms for the robot to reason about the mental state of its human partners
* Also, it is able to generates, monitors and takes part to human-robot shared plans

# 2019-04-11 #
* I continue my learning progress on phyton programming
* I start to understand about classes and iheritance
* I have learn the Basic programming in Python 3 in https://colab.research.google.com/drive/1uj3GEmGEJbJeaZEnJiUW0aTjtddkSK9o#scrollTo=eahdv4tRLcLE
* I watched "But what is a Neural Network? | Deep learning, chapter 1" in https://www.youtube.com/watch?v=aircAruvnKk&feature=youtu.be
* I try read the article "A predictive model for the maintenance of industrial machinery in the context of industry 4.0"

# Article summary #
* it is proposed a predictive model as part of a prognosis system that takes advantage of the core technologies behind the Industry 4.0 paradigm
* The workflow of such system is as follows: sensors mounted on the machinery (Cyber–Physical Systems), produce data that are networked (IoT), stored and managed (Big Data) to inform the operators about the machinery state (IoS)
* a predictive model estimates the machinery condition and is able to predict its behavior after the execution of a number of processes, assisting operators to make informed decisions regarding scheduling.
* the ability of the model for learning from new data as been assessed in an emulated ongoing evaluation over time.

# 2019-04-18 #
* I received emails regarding the course
* I still need to working with my programming skill in Python 3.
* I continued to watch the "But what is a Neural Network? | Deep learning, chapter 1" in https://www.youtube.com/watch?v=aircAruvnKk&feature=youtu.be"
* I try to read the article "Creativity and artificial intelligence"

# Article summary #
* Only a few AI-models can critically judge their own original ideas
* And hardly any can combine evaluation with transformation.
* The ultimate vindication of AI-creativity would be a program that generated novel ideas which initially perplexed or even repelled us
* We are a very long way from that

# 2019-04-25 #
* I read Ch. 5.1-5.3 in Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning
* I still tried to review everything that I've learn and start to learn introduction to tensor flow

# 2019-04-25 #
* I review Ch. 5.1-5.3 in Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning
* I still tried to review everything that I've learn and start to learn introduction to tensor flow
* I have contacts of the group member, try to arrange things on group projects

# 2019-05-13 #
* So we have an physicall class this week. Is it more clearer for me to learn from physicall class.
* This week, I learned about the bias of algorithm
* I found that bias of algorithm may result worse accident or case
* Personally, I think that scientist may obey or careless about the social impact of his creation/algortihm. So starts from now, scientist should deeply think about the effect of his creation.
* Just to found out that in US, they and judges also utilize AI to determine the period of imprisonment and to identify the people using facial recognition. Imagine If there are biases!
* Discussing these things is good as brainstroming
* The video about stop button also give me a chill. Scientist or engineering should be careful when designing AI.

# 2019-05-22 #
* Start to working with group about the project
* Finally, we able create the basic model
* Start working on drop and back propagation

# 2019-05-31 #
* I discussed about our model in project this week. I think we made good progress.
* We verified our model again and plan to start making ppt about it next week
* I review what I have learn so far
* My thought on automation is that it is very suitable on detects and gives conclusion about something that may be too complicated or may be too many data that needed to be proceed.
* Also, the automation is very suitable for types of job that required preseverance and speed.
* For example, In manufacturing section, the manufacturer of bottle caps used to control their manufacturing process by using statistics with sampling method.
* In the industry 3.0, manufacturer starts to use the image processing to identify the defected parts. Instead of sampling, they check every produced caps and reject the defects.
* In the next era, Industry 4.0, the system not only reject the defected parts, but also create feedbacks to control the process parameters in injection mold process.
* Small factories with high flexibility and autonomous ability to produced things will be build in many regions instead of one big plant method.
* That is my thought on automation related to my field.

# Article summary #
* For the first time, a pre-trained CNN-VGG16 approach was implemented for H&E-stained histopathological uterine cervix tumors classification.
* The dataset was provided from TCGA data portal and underwent multiple pre-process methods to overcome WSIs conformation obstacles.
* Their experimental findings are remarkable and demonstrate the capability of deep learning pre-trained models toward promising computer-aided diagnosis in digital pathology.
* This study is an initiation to other researches in light of the fact that some cancer patterns can not be gleaned by human examination.
* There is a clear potential of transfer learning in histo-genomicscorrelations on which we intend to work

# 2019-06-07 #
* About group project, we start to consider on model that can identify english letters (alphabets)
* I cannot attend the physical class this week, so I need to catch up on materials that taught on that day
* we review the model this day
* we start to also create the structure of our ppt slides
* Somehow, our model on recognizing alphabets is still has low accuracy