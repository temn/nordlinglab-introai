This diary is written by Mike Phung for the course of Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

It contains the summary of todays lecture on Introduction to Python.

# 2021-03-05 #
  * Review Lecture 2 (2021-03-04)
  * Read chapter 2-5 in the book: Automate the Boring Stuff with Python
  * Read an intersting article, namely, "Demystifying deep learning" (https://bdtechtalks.com/2021/01/28/deep-learning-explainer/?fbclid=IwAR2nASkCv2HXcIzUz8SliWaVh_9FXsl1-rRvR0W0E_31YMhEW4YG6DkdjR8)

# 2021-03-11 # 
  * Using python 3.0 in Google colab (edit, save, import enironment)
  * Coding for basic program using numpy, math, matplotlib 
  * Asking help from Colab
  
# 2021-03-18 #
  * Practicing Python on Jupiterlab
  * Printing text with quote, double quotes, slashdashes
  * Using loop, varible, function
  * Printing a list, array, astype a variable
  * Learning how to read an advanced python code with object oriented programming in class, function definie other function

# 2021-03-25 #
  * Step into Machine Learning theory; neural network
  * Understading how deep neural network work
  * Practice more in Keras, Numpy, and Pandas for dealing with data input
  * Reading algorithms for training neural network: Why is stochastic gradient descent mostly used in training Neural network?
  
# 2021-04-08 #
  * Learning the TensorFlow, Keras and deep learning lecture of Martin Gorner
  * Building a basic neural network, understand structure of the deep learning network
  * Adjusting strucure in the network model such as adding Dropout layers, more Dense layer to increase accuracy and reduce overfilting
  * Homework: watch the lecture: "Deep Learning Basics: Introduction and Overview" by Prof. Lex Fridman at MIT
  * Video link: https://www.youtube.com/watch?v=O5xeyoRL95U
  * Review the deep learing video: Great resource!
       + The lecture shows an overview of ecosystem in  deep learing with practical examples
       + Github code provides throughout the tutorial of deep learing network: https://github.com/lexfridman
       + Discuss challenges for supervised learning 
  
  