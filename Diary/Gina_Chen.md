This diary is written by Gina Chen, H44071162

# 2019-02-20 #

* I want to know more about Turing Test.
* I learned that there are many projects applying AI technic beside AlphaGo.
* I learned that AlphaGo is so famous because it can play with only basic datas, and become better by playing with human.


# 2019-02-27 #

* I want to know more about System Identification.
* I don't understand the white, grey and black boxes part.
* I like the overview about AI, Machine Learning and Data Science.
* I like the picture telling how different fields are related to each other.
* I learned that Go is a kind of reinforcement of Machine Learning.


# 2019-03-06 #

* Hello World!
* I learned basic Python language.
* If I want ' or " print out, I should put \ before it.
* In Python ' and " are the same.
* I wonder what a boolean is.

# 2019-03-13 #

* Today we practiced Python in group.
* I think it is great to practice with a partner, because I can ask her questions I faced when practicing.
* I wanted to find out the difference between string and float, so I multiplied a string by 100. Then I got that string for a hundred times.
* I couldn't perform the "if,elif,else conditional statements" in the Python I had downloaded.
* I want to know more about array.

# 2019-03-20 #

* Today we learned Neural Network.
* I learned the difference between Classification and Regression.
* Wights are numbers that help us finding out whether there is a edge of lighted up pixels.
* Organize weights as a matrix makes the calculation process easier.
* On 3-21, google doodle use Ai technic to make melody in Bach's signature style.

# 2019-03-27 #

* Today we learned about under- and overfitting.
* A model is always wrong, since it will become exactly the truth if it is right.
* I learned about training error and validation error.
* We should stop training when validation error starts to increase.
* Sigmoid squishes the relevant weighted sum into interval between zero and one, 
   while ReLU(rectifid linear function) gives output up to infinite if not zero.

# 2019-04-10 #

* If we find the global minimum, over fitting problem may occurs.
<<<<<<< HEAD
<<<<<<< HEAD
* We should find the most suitable step when doing Gradient descent.
* I wonder what local pattern and global pattern are?
* Weights and bias decide what patterns filters will detect.

# 2019-04-17 #

* A local pattern is a specific kind of pattern decided by a filter, while a global pattern is the whole picture. 
   For example, if the global pattern is a face, the local pattern may be a nose.
* A model is useful for what it is trained for, but can't do things other than that.
* It may takes twenty to forty years from now to develope model for multi-function.
* With tools provided by companies like TensorFlow and Google, we can train a model in a few step.

# 2019-04-24 #

* Today we tried to train our own model during the class.
* I know what were those codes for, however, I couldn't write them on my own.
* It is good enough for me to know something about ai as I only spend an hour a week to learn it.

# 2019-05-01 #

* We discussed the problem we had for trainning our model in class.
* There is a limitation on how many hidden layers we can have when using sigmoid, while relu don't cause this problem.
* When describing input graph, 1 stand for black and white, while 3 means red,green and blue.
* I want to know more about optimizer.
* Adding another layer may make the model perform worse.

# 2019-05-08 #

* I like the analogy of using ai coding packages without knowing how to write them and using a smartphone without knowing how it works.
* A Neural Network is a general function approximator.
* Backpropagation is used to change weights of models.
* We discussed problems that classmates faced in class today.

# 2019-05-15 #

* We dicussed about how ai technic will change labor market in the future.
* The effet of ai technic will be so extensive that most of the jobs today will no longer be available in the futuer.
* The speed of development on ai technic is much faster than predicted.
* It is worth discussion that if there is a industrial 4.0 going on, since the way we move haven't been changed.
* In my opinion, automation is not avoidable in the future and can reduce lots of routine work.
* I want to work as a financial analyst in the future.

# 2019-05-22 #

* We talked about ethical problems that ai technic may cause.
* It really shocked me that with the help of ai technic, we can make a fake video with someone's facial data.
* If those fake videos were to be made in the future, we can no longer take videos as evidence.
* Algorithm bias will make a model go to a extrem. For example, a facial identify machine may not work on people with darker skin 
  and a picture identify model may report 'magnificent' when given a picture of a fire.
* I think there must be a person making final decission for madical treatment and for lawsuit judgment.

# 2019-05-29 #

* We worked on our group projects in the class.
* I should find out what voting is.
* I couldn't catch up on others in my group, they really knows a lot about coding.
* Working as a group is great, I learned a lot from my group menbers.

# 2019-06-05 #

* We still worked on our model during the class today.
* We thout adding one more dropconnect layer will make the result better.
* A drop connect layer can reduse over-fitting.

# 2019-06-12 #

* Four groups have done their group presentation today, all the models they trained have accuracy up to 99%.
* They tried many ways to train their mmodel, such as max pooling.
* Our best result was only 98%, maybe we should try CNN.
* We are trying to add some more dropconnect layers, since the models we trained are still over-fitting.

# 2019-06-19 #

* I was surprised that there is only one class about engineering in English.
* I learned a lot in this class and I am glad that I have joined the class.
* I should work harder to keep on others.
* We should find the most suitable step when doing Gradient descent.
* I wonder what are local pattern and global pattern ?
* Weights and bias decide what patterns filters will detect.