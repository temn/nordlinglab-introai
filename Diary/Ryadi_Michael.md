This diary file is written by Michael Vashni Immanuel Ryadi P68097098 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2021-02-25 #
* This is the first time, I joined the class.
* Prof. Nordling ways of teaching is extraordinary and amazing for me. He encourage the student to has interaction which is quiet rare for me since I come to Taiwan
* Find success stories from journal assignment.

# 2021-03-04 #
* I could not attend the class because I need to go Taitung for my lab research work
* I only read the material uploaded in the Git and the website link
* AI is interesting. It would help the human solving hard task. However, solving easy task for AI it is tricky

# 2021-03-11 #
* Short Programming course in Python languange
* Learning about using Google Collab to code. Previously, I only use text editor like Atom to code in Python. Interesting tool

# 2021-03-18 #
* Learning about how to use Jupyter Notebook. This is an interesting platform to code in Python languange
* Further explanation about printing, comment, class and function code in Python

# 2021-03-25 #
* Learning about basic of neural network
* Listening the success stories from the other students. It makes me to wonder when the AI can outperform the human as Prof. Nordling mention it.

# 2021-04-08 #
* Watching the example of Tensorflow implementation on simple Neural Network
* There is open dataset that can be used as training data. MNIST dataset

# 2021-04-15 #
* Learning the basic principal of Neural Network and how to understand the limitation of Neural Network
* There is no perfect neural network model for all purposes

# 2021-04-22 #
* Learning the basic principal of Backpropagation, Gradient descent and stochastic gradient descent.
* Learning about simple loss function
* Learning important math background in Neural Network. So it would help us in the future dealing with the result of Neural Network

# 2021-04-29 #
* Learning the basic principal of several Loss function.
* Learning the important idea in Dropout

# 2021-05-06 #
* Learning the basic principal of Convolution Neural Network
* CNN is a sparse fully connected network which lowered down the number of freedom parameter

# 2021-05-13 #
* Learning the basic principal how to validate the result properly. From the result, two parameters that can be compared are bias and variance

# 2021-05-20 #
* Learning the ethics on judging the machine learning result
* AI can prdouce serious problem and lethal if it used brutally for example fake AI

# 2021-05-27 #
* Self Learning about loss function in Udemy
* Starting to implement the MNIST dataset for group project using CNN and get excellent result. But, we want to improve it until limit.

# 2021-06-03 #
* Automation work is a new challenge that will affect human lives very soon. 
* There is no job that really safe from "automation" possibility
* The other interesting point is human always find the gap to fulfill their existence

# 2021-06-10 # 
* Listening to the example of AI research on various application presented by other classmates

# 2021-06-17 #
* Our group hold a discussion on Wednesday (June 16th) with a student in Material Science and Engineering Department, NCKU who currently is working in AI.
* From the discussion, I got a clear insight that the number of hidden layer is highly related to the parameter that need to be solved which highly related to the number of the data.
* Finalizing our group project on Sunday(June 13th)

# Article Summary #
* Article : [Ringland2019_Characterization of food cultivation along_Ryadi_P68097098](https://www.sciencedirect.com/science/article/pii/S016816991831144X?via%3Dihub)
* The paper is applied deep learning network on Google Street View images to characterize the plant type using Inception v3 model
* The author applied two classification. First, a multiclass crop classifier to classify the image to 27 classes. Second, binary classifier to classify the images to banana and non banana.
* The paper shows that the trained deep learning able to classify with accuracy of 83.3% on the entire set of test images and 99.0% on the 40% iamges was most confident classification.
* The paper shows that the binary classifier has value 0.9905 Area under Curve which indicates excellent classifier performance