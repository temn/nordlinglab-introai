This diary file is written by Eric Tsai B34051018 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-18 #

* This is the first day of my diary, and it's also the first time to use bitbucket. I spend some time figuring out how this works.
* I searched for some sucessful AI stories, and the robot in Incheon Airport, South Korea came in my mind.
* I uploaded my slides of a sucessful AI story.
* I find it quite interesting, but I still know little about AI
* I'm looking foward to learn more this semester!!!


# 2020-03-25 #

* I read some AI information on the website https://course.elementsofai.com/1/1.
* the AI related issue that I was facsinated by is the self driving car.
* self driving cars is by no means something that will dominate the future. 
* The department of TCM in NCKU had a test of self driving car in the campus last year. I had a ride on it. 
* Although the speed is only approximately 5 kilometers per hour, I believe it is still a huge forward in the development of AI
* the establishment of self driving car can reduce traffic accidents, giving a better envirnment on road for both vehicles and people.
* I consider the AI develpoments of self driving car is also related to the technology of 5G.
* The last part "Philisophy of AI" also plays a crutial part. 
* Whether the the AI stuff is acting in intelligence, or is it has even conscious, is something we must deal with in future developments.
* Will AI one day really replace mankind?
# 2020-04-01 #
* I looked at the materials of python the professer posted on Bitbucket.
* I take humanity-related courses in universities, and I had little knowledge on writing programs.
* I only had basic knowledge of the program C++, and i barely remember anything.
* Therefore, the materials of python is quite difficult for me. I find it hard to browse it myself and learn something from it.
* But i'll still to try my best to learn those skills.
* After finishing it, perhaps I only understand about 30 percent of the materials.
# 2020-04-08 #
* I watched the video on Youtube 'what is a neural network', and tried to understand the logics in it.
* I read 5-1 to 5-3 from the materials the professor gave. 
* To be honest, I didnt finish "all" of them, because I dom't have some background knowledge, and I found it hard to study on my own.
* I'm really looking forward to the lecture videos to be done, and therefore I can have a thorough learning.

# 2020-04-15 #
* I missed the materials for this week,sorry about that.
* I'll catch up to the ongoing schedule.

# 2020-04-22 #
* I read " using neural nets to recognize handwritten digits.
* A perception takes several binary inputs and produces a single binary output.
* I'm trying to figure out how neurons work.
* I watched the Youtube video about how the Convolutional networks work.
* How filtering works, becoming a map; how pooling works.
* Last is the gradient descent, to see how the errors change.



