This diary file is written by Mohamad Bagas Setiawan P68097056 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2021-02-25 #

* I could not attend the first lecture because of the COVID-19 Pandemic. So, I take the Intro AI and DL course online from Indonesia.
* I just read the material from the bitbucket and I watched "AlphaGo - The Movie". The video makes me amazed with the AI that can defeat humans in a very complicated game.
* Besides that, I know about the information, outline, and policy of the course from the slide.
* After I read the success story of AI, I am interested to know more about the use of AI, especially for surveying or point cloud data processing.
* I wish I can join the class soon because it is difficult to join the course online.

# 2021-03-04 #

* I am still could not join the class due to the COVID-19 Pandemic.
* I am still just can read the materials from the link on bitbucket.
* From the website I have read, I am a little bit understand that it is difficult to determine the AI definition because there is no exact definition about it.
* There are some related topics to AI. These include machine learning, deep learning, data science, and robotics.
* There are some examples of AI to know the philosophy of AI.
* From the material, I am a little bit know the difference between Statistics, Data Science, Data Mining, Artificial Intelligent, Machine Learning, and Deep Learning.
* But I am still can not clearly understand the System Identification means from the 9th slide. There is a White box, Grey box, and Black box.
* I feel so bad because I can not write the diary on time, also I am still confused by the online class system.
* Thank you to the TA which always keeping me update about the class. I hope I can join the class as soon as possible after I have my flight schedule, and I hope it will be at the end of Mach 2021.

# 2021-03-11 #

* Learning the basic information of Python using Google Collaboratory.
* For the third week, I still could not join the class. But my friend Michael Vashni helps me record the explanation. So, I can follow the explanations of the material.
* I think the class is so easy to catch because the explanation is obvious.
* After this class, I know that the real purpose of the command print() always used at the beginning of every training was to check the environment setup is working properly.
* Using the help() command or the question marker (?) to find more information about modules and functions in Python.
* Some modules commonly use in Engineering Field like NumPy, SciPy, MatplotLib, pandas, scikit-learn, and TensorFlow.
* And for the last but not least, to help the reader understand the code we made, we can use the "#" at the beginning of the line to make comments.

# 2021-03-18 #

* I am still have to join the class online
* Continuing the last class about the Introduction of Python
* I just try to install the Anaconda but I still use the Google Collaboratory for the excercise
* It is take a long time for me to understand the material because I just read the power point and try the code for myself
* All thanks to my friend Michael who record the class, so I can watch the recording video and make me a little bit understand about the material
* I hope I can go to Taiwan as soon as possible for take the offline class, so I can more understand and have a discussion with the classmate

# 2021-03-25 #

* I am still have to join the class online. Thanks to Michael who record the class, so I can a little bit understand the materials
* Still continuing the Introduction of Python.
* I can understand the basic write and read a file in Python and now I know about the error which can happen in Python.
* Continuing the material of the Neural Network.
* I can understand a little bit about how Neural Network is working in detecting number from the video.
* Neural Network will use layer to filter the data to determine the result that the user wanted.
* Then, I know about machine learning. There are three basic learning type (Unsupervised, Supervised, and Reinforcement Learning)

# 2021-04-08 #

* I am still have to join the class online. Thanks to Michael who record the class, so I can a little bit understand the materials
* This class continuing to learn the basic of tensorflow.
* I think the video makes me a little bit understand about one of the tensorflow usage.
* I am still confusing in running the code because the recorded video does not clear, so I can not see the practical step.
* I think the class this week is a little bit boring, it is because I just read the code. I think to understand the code, I need to write it in my own.
* Now I already in Taiwan and still at the quarantine day. Hope I can join the class as soon as possible.

# 2021-04-15 #

* I am still can not join the offline class. Thanks to my friend Michael who record video.
* History of artificial intelligence and why deep learning can use many hidden layer.
* The problem in AI is to enable to changes the objectives function. 
* Machine Learning basics are samples and features. The most fundamental thing in machine learning is the data.
* In machine learning there are 2 different problems, that are classification and regression.
* The goal of machine learning is to find the best algorithm based on what task we want to do.
* Learning type defines how to use the data. There are many learning types.

# 2021-04-22 #

* The way we train neural network is based on backpropagation. It use to update the weight on the neuron.
* Backpropagation is to change the parameter of the model and validation is used to evaluate the model to know how good the model fit to other data.
* There are some optimization methods (SGD, RMSProp, Adam, etc)
* The real dataset always contains noises, but the datasets is fix once we have done the measurement so it will be deterministics. When we have a new measurement, we will have a different value and it is will be stochastics
* SGD algorithm use the subset of the full data and always change the datasets to build the model.
* Why we do the stochastics is to reduce the save computation and also to avoid getting stuck in local minima.

# 2021-04-29 #

* The first day that I join the offline class and I am still need to adapt.
* I learn about learning rate, the learning rate need to have an optimum rate.
* Besides that, I also learn about the Loss Function.
* There are many methods in Loss Function (ex: Binary Crossentropy, Huber, Kullback-Leibler Divergence, etc)
* I learn dropout, normalization and activation function too.

# 2021-05-06 #

* I am still confusing about the inception modules.
* In other side, in this class I learn about the Convolutional Neural Network.
* The example of CNN is for image classification process.
* From this material I know that CNN is subset of fully connected network.
* We are not always using fully connected network because fully connected network will be an expensive calculation.
* Then, I learn about the teacher-student strategy to make the training more simple.

# 2021-05-13 #

* I am not attend this class because of the Moslem Eidl Fitri Holiday so I must pray this day.
* For this lecture, my friend Michael told me that there is an explanation about the group project and the final exam.
* Then continuing the material from the last week, the introduction of Model validation that there are some parameters test/training split, cross-validation, under-/overfitting, and bias variance tradeoff
* And the last is an example of Neural Network use in Biological application.
* I have a trouble to know the terms that use in the example because I have not learn about Biology anymore. So, I need more time to understand the purpose.

# 2021-05-20 #

* Going back to online again.
* First, in this lecture I am learning that we need to find the way to give the AI algorithm has common sense.
* Then, in the class we share the personal story about algorithm bias and where the human needs to take the decision for the problem.
* The example of the algorithm bias from discussion is the different decision in different baby's ultrasonic scan. The human needs to decide the true decision.
* The AI capability to produce fake videos close to real video can be dangerous things. Example: How if the AI produce fake video about Dr. Anthony Vauci said that COVID-19 vaccine will cause cancer and there is no people picking vaccine anymore.

# 2021-05-27 #

* No class today.
* I had a discussion with my group member (Pao Tsung) online.
* I was discussed with my group member (Pao Tsung) about the training and test data.
* Besides that, we also still looking for the model that can improve the accuracy of our training.

# Article Summary #

* [Waldner, Francois & Diakogiannis, Foivos. (2020). Deep learning on edge: Extracting field boundaries from satellite images with a convolutional neural network](https://arxiv.org/abs/1910.12023)
* This paper intends to do automatic extraction of field boundaries from satellite imagery, The ResUNet-a model was used in this research because this method has been previously shown to outperform state-of-the-art architectures for semantic segmentation.
* The paper implemented two basic architectures that differ in depth (ResUNet-a D6 and ResUNet-a D7). The Tanimoto distance with complements (a variant of the Dice loss) that achieves faster training convergence than other loss functions irrespective of the weights initialisation and keeps a good balance across multiple tasks.
* They used two-step optimising process (First,  the threshold for the extent mask is defined by maximising the Matthew's correlation coefficient and the second is the thresholds of the boundary and distance masks are jointly optimised).
* The model's performance and its ability to generalise greatly improve in this paper by asking the algorithm to reconstruct three correlated outputs. when validated against the test data, the best D6 model was the one trained with highest weight decay performed best (MCC = 0.81). But overall, ResUNet-a D7, the deeper model, yielded the highest accuracy (MCC = 0.82).

# 2021-06-03 #
* The first thing discussed in today's class was about the explanation about group project. Professor re-explain about the group project.
* Today's topic was about Automation, disruptive innovation, and the future of work.
* From today's class I know about the history of the industrial revolution, there are many definition about the industrial revolution. But, in the end we can devide the industrial revolution into three generations.
* We can devide the industrial revolution from three aspects there are COMMUNICATE+MANAGE, POWER+FUEL, and MOVE. The latest industrial revolution is the 3rd Industrial Revolution (from the developtment of internet, renewable energy, and autonomous vehicle)
* The new innovation can affect the economic activity. In this time there are five major innovations platform (Artificial Intelligence, Energy storage, robotics, genome sequencing, and blockchain technology) that have a big impact in the economic activity.
* When the revolution is starting, there are many jobs can handled by the AI or robot. It can reduce the the human resource needs and makes many people lost their jobs if they can not adapt to the revolution.
* After the class, I discussed with my friend about the group project. We were trying to input our own data, but it still does not work and have an error.
* So, we just using the mnist datasets for our project. And still trying to make improvement in accuracy.

# 2021-06-10 #

* We disscuss about the current AI research in today's class.
* Students were have a live presentation or using the recorded video to present the Article summary of the current AI research.
* In today's class, I already record a presentation video. So, we just watch the video.
* Prof. Nordling asked me about the accuracy in training and the fluctuation in the accuracy chart, the accuracy is still low but I think it still can be usefull in the field boundary detection.
* Jacob also had a question "is the methods have compared using another methods?". The answer is not, because they just comparing the same architecture but using a different number of layer (6 layer and 7 layer).
* I think I had start to confuse to read the bitbucket because the class schedule is not clear. Because I heard in the last class professor said that we will meet in 2 weeks but the in the bitbucket is written that today we have a class.

# 2021-06-17 #

* Today I have another discussion about our group project online.
* Me and my friend found that our validation accuracy higher than the test accuracy, but we still looking for the solution. (validation accuracy = 96.060% with error rate 3.940% and training accuracy = 0.4934 with loss = 1.3215.
* We already tried several combination of optimizer, dropout value, activation function, and loss function. Adam optimizer, relu and softmax as activation function, dropout value 0.5 and sparse_categorical_crossentropy as the loss function can give better result so far.
* We want to try to use CNN to upgrade our accuracy both in training and validation.
* So far, we still need to adjust the value to have a better result and looking for explanation why is the validation accuracy was higher than the training.

# Final Group Project #

* After we have a discussion, we determine to use CNN model to finished our group project and we can reach Training accuracy 99.74% and Validation accuracy 99.22%.
* Here is the link of the video [MNIST_0.78%_PaoTsung_MohamadBagasSetiawan_Group2](https://youtu.be/ikdgGOh5RBE)

