# List of AI success story topics #
 
Here are the topics covered so far. 

Author: 陸皓邦 Hao-Bang Lu
Date: Mar 18, 2020. (I listed them but it'll change)

-------------------------------2019-------------------------------

1. Ai & Law
2. Melomics
3. Art and style imitation
4. AlphaStar: DeepMind’s Starcraft II A.I.
5. AI, a way to revolutionizes our education
6. AI and Human Resource Management
7. Artificial Intelligence Outperforms Human Data Scientists
8. How would you recommend one out of more than 40 Million products?
9. AI Playing Rubik’s Cube
10. Success stories where an Artificial Intelligence outperform humans
11. Website Design Modifications with AI
12. Artificial Intelligence in molecular design
13. Libratus
14. Robotic reporter
15. AI Physicist: derive the law
16. Which one fits me better?AI Dressing Mirror
17. The Application of AI in Dota2
18. AI in Media
19. AirAsia’s goal
20. Forecasting Future Harvests Using Artificial Intelligence
21. AI in Monument restoration
22. Robocop
23. An AI Program that Composes Pop Music
24. The application of AI in NBA
25. Google Ads using AI for Optimized Campaign: How AI Help Advertiser to Write Their Ads: Rio Ananta Perangin-Angin
26. Ai The film editors’ helper.
27. McCormick collaborate with IBM’s machine
28. AI Success Story : THE FUTURE OF OIL AND GAS, POWERED BY AI AND GPUs
29. AI beats humans in Stanford reading comprehension test
30. Successful story of AI: Netflix

-------------------------------2020-------------------------------

31. AI 'outperforms' doctors diagnosing breast cancer
32. Artificial Intelligence Approach for Identification of Diseases through Gene Mapping
33. Neural Approach to Automated Essay Scoring
34. Successful story of AI: Autopilot of Cars
35. AI Success Story of Siri
36. AI on Satellite Imaging
37. Using AI to Interpret Human Emotions
38. AI diagnose breast cancer
39. Lung Nodule Detection
40. Detection, Segmentation and Recognition of Face and its Features Using Neural Network
41. AI in Forensic Science
42. AI finds vaccine
43. AI in agriculture (plantix)
44. AI in digital pathology
45. Growing Neural Cellular Automata
46. BabyX
47. AI in Customer Service - Salesforce Einstein.
48. AI in Astrophysics
49. Predictive Maintenance of Machine Tool Systems Using Artificial Intelligence Techniques Applied to Machine Condition Data
50. AI in healthcare industry
51. AI driven scanning probe microscopy
52. Google Nowcast
53. AI Detects Lung Cancer Better Than Radiologist.
54. AI in Google Maps
55. AI in the Car Recognition Identification
56. MedCheX - An e-Alert system for automatically detecting pneumonia from chest x-rays
57. How Google automatically  train neural net work
58. AI nose: Learning to smell
59. How AI Performs Better In Plastic Injection Moulding Fields

-------------------------------2021-------------------------------

60. AI on forest point cloud data (https://drive.google.com/file/d/1WYmMtilO9pUs5SbZMRkN-XXJmpdahbgo/view?usp=sharing)
61. AI in Gene Editing (https://docs.google.com/presentation/d/15zWiOueAY8t5hEb5tXKffBrXG7HeLm5W/edit#slide=id.p1)
62. AESOP： Strengthen medication safety with AI(https://drive.google.com/file/d/1ONerjmwYkavnLAcMmG2cgjTgsoMWqYK2/view?usp=sharing)
63. AI_in_Car_TVprogram_Knight_Rider (https://drive.google.com/file/d/1RE0bKbNqV1vJAZ00ohb6OIS7IOi1c3bM/view?usp=sharing)
64. Artificial Intelligence in Healthcare success story (https://docs.google.com/presentation/d/1WvXLYQsp5Symg8hj6mXn6GM6SMF2CWQix2IIyP1GLwQ/edit?usp=sharing)
65. Success story of Photomath (https://drive.google.com/file/d/18G_pu4IYJQKK_E1z816l1e6fnRlstaJ2/view?usp=sharing)
66. Autonomous litter surveying and human activity monitoring (https://drive.google.com/file/d/1ixsvTOJei4HjhJ9QvX_UOGWjQdGob7Ss/view?usp=sharing)
67. [Autonomous litter surveying and human activity monitoring](https://drive.google.com/file/d/1ixsvTOJei4HjhJ9QvX_UOGWjQdGob7Ss/view?usp=sharing)
68. [AI in litter surveying and human activity monitoring](https://drive.google.com/file/d/1ixsvTOJei4HjhJ9QvX_UOGWjQdGob7Ss/view?usp=sharing)
69. Artificial Intelligence in Healthcare success story (https://docs.google.com/presentation/d/1WvXLYQsp5Symg8hj6mXn6GM6SMF2CWQix2IIyP1GLwQ/edit?usp=sharing)
70. AESOP： Strengthen medication safety with AI(https://drive.google.com/file/d/1ONerjmwYkavnLAcMmG2cgjTgsoMWqYK2/view?usp=sharing)
71. [Autonomous litter surveying and human activity monitoring](https://drive.google.com/file/d/1ixsvTOJei4HjhJ9QvX_UOGWjQdGob7Ss/view?usp=sharing)
72. AI_in_Car_TVprogram_Knight_Rider (https://drive.google.com/file/d/1RE0bKbNqV1vJAZ00ohb6OIS7IOi1c3bM/view?usp=sharing)

