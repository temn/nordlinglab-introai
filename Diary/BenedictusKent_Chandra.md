This diary file is written by Benedictus Kent Chandra F74075055 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-21 #

* It amuses me how fast NCKU responded on the newly reported Covid-19 case found here.
* Read an article about the application of AI in NBA to do the homework. Never thought that even NBA professional teams need AI to help them asess the game situation.
* Need to get used to work with this website to do stuffs. I am still quite slow to understand where to find all the material.
* So grateful that I found TA's note on moodle, he made it so much easier for me.

# 2020-03-28 #
* Apparently there is no clear definition of AI. The two words closest to AI is autonomy and adaptivity.
* What seems easy to us is hard for computers, but what seems for hard for us is actually easy for computers.
* Deep Learning -> Machine Learning -> AI -> Computer Science (-> means subfield of).
* Turing Test: If interrogator can't distinguish whether A or B is a computer, the computer is said to pass the test.
* The Turing Test is constantly being criticized as "not complete", but I think the more people criticize the better the test will be.
* Intelligent behaviour of system is not equal to actually being intelligent.
* All AI methods we have today falls into "narrow" AI, which only handles one task.
* The idea of "narrow" and general AI kind of blew my mind.
* Never in my mind that the researcher have already abandon the idea of AI in sci-fi movies. I thought we are already so much closer than before.

# 2020-04-05 #
* Not sure whether there is indeed no resources to look at this week or I just don't understand fully how this website works.
* Tried to study Python on my own.
* Used Sublime IDE as my text editor since it gives colors to tags other IDE don't.
* Learnt some things on Youtube like printing, variables, list, etc.
* Tried to do text version of Tic Tac Toe. It failed. No clue what went wrong.
* Unlike C or C++ that uses curly brackets ("{ }") for statements, Python uses colon (" : ").
* Python was not as hard as I thought.

# 2020-04-13 #
* Forgot to write last week's diary. Realized I was late when it was already 1 AM on Monday.
* Learned about how to define a function.
* Function is defined with def Function(). It is still part of function as long as there is tab.
* Function can be called multiple times by writing the function name, e.g. Function().
* Apparently Python is able to return multiple variables, solving the problems many programming languange can't.
* Multiple return values can be stored in variable by using multiple variable in a single assignment.
* Errors: Compile Time, Logical, and Run Time
* Error handling usually means handling a run time error. Can be handled with try except block.
* Never knew there is such thing as "finally" block.
* File handling always starts by opening the actual data by using open('File_Name', 'mode') command.
* Mode can be r (reading), w (writing), a (append), r+ (read and write).
* Can be put into a variable.
* To read use read(), read line use readline(), etc.
* To write use write().

# 2020-04-19 #
* Solved some bugs on my tic tac toe game, but new bugs appear again.
* Read the Python Deep Learning on Google Colab. A little confused on the class and inheritance part (specifically in the code: self.entry and so on).
* Tried to install Tensorflow using Anaconda but doesn't seem to be working just yet, even though I have followed the tutorial on YouTube.
* Watched the neural network video.
* Realized that all this neural network thing might be harder than I thought.
* The purpose of using neural network is to find patterns from seemingly random data.
* Activation: Numbers from 0 to 1 according to how black or white the picture is.
* Can input a bias number into the sigmoid function to calculate activation value into the next layer of the network.
* Learning: Finding the right weights and biases.
* Heavily related to Linear Algebra.

# 2020-04-26 #
* Watched the Tensorflow video on YouTube.
* Neuron's job is to do weighted sum of all pixels and bias (additional degree of freedom).
* Y = softmax(X.W + b)
* placeholder, the function to hold the training image. Variable, the function for Python to compute the weighted sum.
* Kinda understand what the professor is talking about, but for sure I can't write it on my own.

# 2020-05-03 #
* Dedicated the whole week to study for my next week's midterm exam.
* Didn't type even a single line of code.

# 2020-05-08 #
* Written a deep learning code with help from YouTube using my own Google Colab.
* As of today, still not sure how it actually works.
* Only understand some part of it, but still not very intuitive.
* Wrote a deep learning on MNIST, which is predicting number by 28x28 image.

# 2020-05-17 #
* Start tinkering with materials (codelab) by Prof. Martin Gorner.
* The file on third section when run, there is an error although it seems like there shouldn't be an error.
```
AttributeError                            Traceback (most recent call last)
<ipython-input-4-6031f6ed1815> in <module>()
      5 import tensorflow as tf
      6 from matplotlib import pyplot as plt
----> 7 tf.enable_eager_execution()
      8 print("Tensorflow version " + tf.__version__)

AttributeError: module 'tensorflow' has no attribute 'enable_eager_execution'
```
* Understood the word "epochs" (which means iteration throught entire dataset).
* As accuracy in data training increases, the "loss" function decreases.
* Apparently "tensors" is basically just a multi-dimensional tables/array.

# 2020-05-24 #
* Made the project group chat.
* Going to work on the project this coming Wednesday with the whole group.
* Did nothing much, other than preparing for tommorow's exam.

# 2020-06-01 #
* Forgot to write last week's diary.
* Solved the error on Prof. Martin's code.
* Gathered together as a team and discussed about the project.
* Finished compiling Prof. Martin's code.

# 2020-06-07 #
* Went to the class this week and it was an interesting lecture.
* We are going to head into the future where a lot of jobs are going to disappear.
* The disappear of the jobs are all caused by the potential of computer automization.
* Being a programmer is almost a safe bet to secure a job.
* Due to this COVID-19, the level of pollutions dropped significantly and according to the professor, we might not be able to reach last year's pollution level.
* More electric vehicles are going to appear and become more and more affordable.

# 2020-06-10 #
* Tried to actually read the codes by Prof. Martin Gorner.
* Understood and clear so many understandings by discussing with my friend. Makes reading the explanation by the prof far more easier to digest.
* Tried to understand every line of code in the Google Colab. Some syntaxes are unclear to me whether it is part of Python's or Tensorflow's syntax.
* While reading, realized the prof hides so many things inside visualization utilities block.

# 2020-06-16 #
* Finished with the codes.
* Understood most of it already.
* I thought I understand the concept behind convolutional network, but a friend of mine (doctoral degree student) told me that my concept was wrong.
* I'm a bit confused where I am wrong since a lot of articles on Google seems to agree with the idea of concept.
* Gathered together with the team to discuss more and fill in gaps with each other.
* Learnt so many things, especially regarding the trendline of graph that may show the trend of overfitting.
* Gather to record the project.

# 2020-06-17 #

* Group project：https://www.youtube.com/watch?v=d4mvKBoAoNI
* Meow~(=^-ω-^=)