# 2020-06-14 #
*  Prerecorded YouTube video of the presentation of group project: https://youtu.be/U9WCIDw2rlo

# 2020-06-07 #
* Watched the video "Deep Learning State of the Art (2020) | MIT Deep Learning Series" on YouTube.
* Watched the video "Tesla Autonomy Day 2019 - Full Self-Driving Autopilot - Complete Investor Conference Event" on YouTube.
* Working on the group project.
* Read the article "Using neural nets to recognize handwritten digits".

# 2020-05-31 #
* Thoughts about automation: Automation has been part of our modern technology and made our daily lives more convenient. In my opinion, security and accuracy are the cruial aspects to be considered when it comes to automation. How to make a sensor or other devices to receive the input signals intact? How to make the computers do the right decision in time? On the whole, I have a positive outlook on the future of automation.
* My future work plans: I would like to apply deep Learning to my current or future research if it is feasible to enrich and make some innovations on my research.
* Watched the video "The Digital Skills Gap and the Future of Jobs 2020 - The Fundamental Growth Mindset" on TouTube.

# Article summary #
* The summary of "Two-path Deep Semi-supervised Learning for Timely Fake News Detection, by Xishuang Dong, Uboho Victor and Lijun Qian"
* Usually very few data samples can be labeled in a short time, which in turn makes the supervised learning models infeasible.
* In order to achieve timely detection of fake news in social media, a novel framework of two-path deep semi-supervised learning is proposed where one path is for supervised learning and the other is for unsupervised learning.
* The supervisedlearning path learns on the limited amount of labeled data while the unsupervised learning path is able to learn on a huge amount of unlabeled data. Furthermore, these two paths implemented with convolutional neural networks (CNN) are jointly optimized to complete semi-supervised learning.
* The two paths in the model generate supervised loss (cross-entropy) and unsupervised loss (Mean Squared Error), respectively. Then training is performed by jointly optimizing these two losses.
* In addition, we build a shared CNN to extract the low level features on both labeled data and unlabeled data to feed them into these two paths.
* To verify this framework, we implement a Word CNN based semisupervised learning model and test it on two datasets, namely, LIAR and PHEME. Experimental results demonstrate that the model built on the proposed framework can recognize fake news effectively with very few labeled data.

# 2020-05-24 #
* Do group project
* Read "How Convolutional Neural Networks work" on YouTube
* Read Chapter 9 of Deep Learning by Ian Goodfellow et al

# 2020-05-17 #
* Install Tensorflow
* Read Chapter 5 of Deep Learning by Ian Goodfellow et al
* Watched the video "TensorFlow and Deep Learning without a PhD"

# 2020-05-10 #
* Watched the video "What is backpropagation really doing? | Deep learning, chapter 3" and summarized some main points.
* The thing we're looking for is the negative gradient of this cost function, which tells you how you need to change all of the weights and biases, all of these connections so as to efficiently decrease the cost.
* Backprogagation is an algorithm for computing the gradient.
* The magnitude of each component describes how sensitive the cost function is to each weight and bias.
* We can't directly change those activations. We can only adjust weights and biases.
* Increase b -> Increase wi in proportation to ai -> Change ai in proportation to wi
* Hebbian theory: Neurons that fire together wire together. The biggest increases to weights, the biggest strengthening of connections happens between neurons which are the most active and the one which we wish to become more active. 
* Stochastic gradient descent: Randomly shuffle the training data -> Divide it into a whole bunch of mini-batches -> Compute gradient descent step using backprop according to the mini-batch. (It's not going to be the actual gradient of the cost function that depends on all of the training data, not this tiny subset. It's not the most efficient step downhill but each mini batch does give you a pretty good approximation and a significant computational speed up.)

# 2020-05-03 #
* Watched the video "Gradient descent, how neural networks learn | Chapter 2" and summarized some main points.
* Activation: weights in the weighted sum; the strengths of the connections between layers.
* Cost function: To tell the computer how badly the network works by adding up the squares of the differences between each of those trash output activations and the value that you want them to have. This sum is small when the network confidently classifies the image correctly. It's large when the network seems like it doesn't really know what it's doing. The cost function takes as its input those thirteen thousand weights and biases and spits out a single number describing how bad those weights and biases are and the way it's defined depends on the network's behavior over all the tens of thousands of pieces of training data.
* Consider the average cost over all of the tens of thousands of training examples.
* Basically it comes down to finding the minimum of a certain function. If you make your step sizes proportional to the slope, then when the slope is flattening out towards the minimum, your steps get smaller and smaller. This keeps you from overshooting.
* [multivariable calculus] The gradient of a function: the direction of steepest ascent, which means to increase the function most quickly; The negative of the gradient: the direction to step that decreases the function most quickly; The length of the gradient vector: how steep that steepest slope is.
* [algorithm] Compute ∇C => Small step in ∇C direction => Repeat; Start at any old input and figure out which direction you should step to make that output lower. Figure out the slope of the function where you are. If the slope is positive, shift to the left. If the slope is negative, shift to the right. Do this repeatedly at each point and check the new slope, you're gonna approach some local minimum of the function. There are many possible valleys that you might land in. There is no guarantee that the local minimum you land in is gonna be the smallest possible value of the cost function.
* Backpropagation: the algorithm for computing this gradient efficiently.
* Gradient descent: repeatedly nudging an input of a function by some multiple of the negative gradient


# 2020-04-26 #
* Watched the video "But what is a Neural Network? | Deep learning, chapter 1" and summarized some main points.
* Convolutional neural network -> good for image recognition; Long short-term memory network -> good for speech recognition.
* Neuron: holds a number, especially between 0 and 1; A function that takes the outputs of all the neurons in the previous layer and spits out a number between 0 and 1.
* Example: A neural network that can learn to recognize handwritten digits
* 1st layer: A bunch of neurons corresponding to each of the 28x28 pixels of the input image. 
* Last layer: ten neurons, each representing one of the digits. The brightest is the network's choice for what digit this image represents.
* Hidden layers: When recognizing digits we piece together various components. Each layer in the second-to-last layer corresponds with one of these sub-components. The sub-components can further be broken down into finer sub-components.
* Activation: Each one of these holds a number that represents the grayscale value of the corresponding pixel. Ranging from 0 for black pixels up to 1 for white pixels. Each neuron is lit up when its activation is a high number. The way the network operates activations in one layer determines the activations of the next layer. Some groups of neurons firing cause certain others to fire.
* Weight: Assign a weight (number) to each one of the connections between our neuron and the neurons from the first layer. Weights tell you what pixel pattern a neuron in the second layer is picking up on. 
* Take all those activations from the first layer and compute their weighted sum according to these weights.
* When you compute a weighted sum like this you might come out with any number but for this network what we want is for activations to be some value between 0 and 1. So a common thing to do is to pump this weighted sum into some function that squishes the real number line into the range between 0 and 1. 
* The sigmoid/logistic function: very negative inputs end up close to 0; very positive inputs end up close to 1; steadily increases around the input 0. (old-school)
* ReLU(Rectified Linear Unit) function: below threshold -> 0 (inactive), above threshold -> f(a)=a; much easier to train.
* Bias (for inactivity): The number added to the weighted sum before plugging it through the sigmoid function. Bias tells you how high the weighted sum needs to be before the neurons start getting meaningfully active.
* Linear algebra form: a1 = σ(W a0 + b); weights W -> a matrix, activations a0 a1 -> a vector, bias b -> a vector
* Learning -> Finding the right weights and biases


# 2020-04-12 #
* Attended the class this week.
* Downloaded Python and PyCharm today.
* I programmed some simple Pyhon code which included printing of a string, declaration of variables and arrays, conversion of data value and data types, if-else statement, for loops and while loops.
* So far, the basic code I have practiced in Python was similiar to that of in C++, which was a good news for me since I had been already similiar with C++.

# 2020-04-05 #
* Read the article "A visual proof that neural nets can compute any function".
* Watch the video "How computers are learning to be creative | Blaise Agüera y Arcas" on Youtube.

# 2020-03-29 #
* I saw an article which was titled "Machine Learning Confronts the Elephant in the Room".
* It revealed the weakpoint of computer vision systems.
* First, the scene of a living room was processed by the computer vision system and the system detected every object correctly. Next, a picture of an elephant was placed inside the scene. This caused the computer vision system to detect some of the object incorrectly and even some of them were ignored.
* The key factor of this problem lies in how computer vision systems process data by neutral network. It may be a bit of counterintuitive from the perspective of our human beings. A computer vision system will not conduct a second check if the presence of something confuses them since neural networks for object detection work in a feed-forward manner.

# 2020-03-22 #

* Learned the application field of AI. For example, self-driving cars, content recommendation and face recognition.
* Learned how to define AI, not just the way sci-fi does.
* Characteristic to AI: Autonomy and Adaptivity.
* Learn how to distinguish between AI, Machine learning, Deep learning, Robotics, Data science and Computer Science.
* Consider the philosophy of AI. The word "intelligent" is ambiguous sometimes.

# 2020-03-15 #

* Learned the success story of AI in the field of art, design, medicine, healthcare, management, data science, robotics, physics, media, agriculture, image processing and customer service.
