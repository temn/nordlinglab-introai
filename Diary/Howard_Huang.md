This diary file is written by Howard, Kuan-Hao Huang (N18044010) in the course of Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-05 (Week1) #
* Due to the COVID-19 epidemic, professor is working on converting this course to an online course for everyone's safety.

# 2020-03-12 (Week2) #
* Professor provides a short welcome message on YouTube: https://youtu.be/-XyF-sJYiDQ.
* Watch video: Success stories where deep neural networks outperform humans presentation video.
* Watch video: Introduction of the digit recognition case study.
* Sign up to participate in the course and get access to the material by registering in Google Sheet.
* Fill in the survey about your background and expectations in Google sheet.

# 2020-03-19 (Week3) #
* Two videos related to success stories of artificial intelligence mislink to `Welcome video` on YouTube.
## A success story where an AI outperform humans
	AI in Forensic Science
	   1. Using AI to Fight Crime
	   2. Crime Scene Investigation
	   3. Identifying Suspects
	   4. Harnessing the Power of Police Databases
## What I have learned from [Elements of AI - Introduction to AI - Chapter 1: What is AI?](https://course.elementsofai.com/1)
## I. How should we define AI?
	A. AI means different things to different people.
       1. AI is about artificial life-forms that can surpass human intelligence.
	   2. Almost any data processing technology can be called AI.
	B. Application
	   1. Self-driving cars
	   2. Content recommendation
	   3. Image and video processing
## II. Related fields
	A. Computer science > AI > Machine learning > Deep learning
	   1. Machine learning means systems that improve their performance in a given task with more and more experience or data.
	   2. Data science includes machine learning and statistics, algorithms, data storage, and web application development.
	   3. Robotics means building and programming robots so that they can operate in complex, real-world scenarios.
## III. Philosophy of AI
	Whether intelligent behavior implies or requires the existence of a mind, and to what extent is consciousness replicable as computation.
	A. The Turing test: A human interrogator interacts with two players, A and B, by exchanging written messages (in a chat).
	   Note. If the interrogator cannot determine which player, A or B, is a computer and which is a human, the computer is said to pass the test.
	B. Criticism of the Turing test: Does being human-like mean you are intelligent?
	   1. Eugene Goostman
	   2. The Chinese room argument
	C. General AI vs Narrow AI
	   1. Narrow AI refers to AI that handles one task.
	   2. General AI, or Artificial General Intelligence (AGI) refers to a machine that can handle any intellectual task.
	D. Strong AI vs Weak AI
	   1. Strong AI would amount to a “mind” that is genuinely intelligent and self-conscious.
	   2. Weak AI is what we actually have, namely systems that exhibit intelligent behaviors despite being “mere“ computers.

# 2020-03-26 (Week4) #
* Review materials that are provided by professor on Bitbucket.
* Find information about how to learn Python.

# 2020-04-02 (Week5) #
* National holiday and spring break.
* Preview material from last year in the links in the README.md file.

# 2020-04-09 (Week6) #
* Make a practice of learning Python via Nordling Lab - Python and Deep learning tutorial in Colaboratory provided last year.
* Watch the 3blue1brown video about visualisation on neural networks.

# 2020-04-16 (Week7) #
* We have a synchronous online lecture this week.
* Professor provides two lecture notes in Google drive.

# 2020-04-23 (Week8) #
* We have a synchronous online lecture this week.
* Click the link of Nordling Lab - Python and Deep Learning Tutorial and do some practice via the website.
* The website provides a good way for me to learn Python.
* It needs some time to run the code cells in order to get the results.

# 2020-04-30 (Week9) #
* I learn different kind of machine learning, including unsupervised learning (unlabelled data), supervised learning (labelled data) and reinforcement learning (alphago).
* I am not sure how to choose loss function for Neural Networks.
* In order to find the minimum of a function, we can apply gradient descent which is a first-order iterative optimization algorithm.
* Backpropagation is one of the important concepts in machine learning, and it is an efficient way to compute the gradient.

# 2020-05-07 (Week10) #
* I learned about the introduction to data preprocessing, model selection, regularization, activation functions, and neural network types this week.
* Professor announced the grouping information of group project this week.

# 2020-05-14 (Week11) #
* We have physical class this week.
* The lecture of this week is mainly about ethics and the danger of algorithm bias.
* We watch four videos related to ethics and the danger of algorithm bias.
* We discuss some questions and share our ideas to the whole class related to the topic in the lecture.

# 2020-05-21 (Week12) #
* Learn how to make the best possible deep neural network for recognition of digits.
* I have heard professor said that "Engineers developing or deploying automation technology have a bright future with plenty of work."
* As a student in the field of mechanical engineering, I believe my job would not be replaced in the future.

# 2020-05-28 (Week13) #
* We have physical class this week.
* We discuss our dream job 10 yrs from now and share to the classmates.
* We think about our work security that we prefer and give some reasons.
* Professor talks about the automation and the future of work.

# Article summary #
## Article Title: [Medical Image Segmentation Using a U-Net type of Architecture](https://arxiv.org/abs/2005.05218) ##
* Deep convolutional neural networks are very effective for analyzing visual image.
* Models like V-Net (for 3D medical image segmentation) and U-Net (for biological microscopy image segmentation) are used for the spcific purpose of medical image segmentation.
* This research proposed a fully supervised FC (fully connected) layers based pixel-wise loss at the bottleneck of the encoder branch of U-Net.
* The results show that the training of two layer based FC sub-net (U-Net type of architecture) contains more information used by the decoder layers to predict the final segmentation map of medical images.
* U-Net type of architecture could produce capable results of image segmentation on MRI and CT scan images.

# 2020-06-04 (Week14) #
* We have physical class this week.
* Today's topic is mainly about automation and employment.
* We discuss how our dream work and desired work security is impacted by automation.

# 2020-06-11 (Week15) #
* We have article summary presentation this week (for master and doctoral version of this course).
* Student who did not make presentation this week has now been scheduled for presentation next Wednesday (June 17th) at 13:10-15:00 in room A1306.
* Next week will be final exam and group project presentation for master and doctoral version of this course.
* Jupyter notebook with both the training and prediction code of group project need to be committed to this GIT repository before the final exam.

# 2020-06-18 (Week16) #
* We have final exam and group presentations for master and doctoral version of this course this week.

# 2020-06-25 (Week17) #
* There are group presentations and final exam for the general education (A93A600 / GE2029) version of this course this week.

# 2020-07-02 (Week18) #
* We don't have class this week.