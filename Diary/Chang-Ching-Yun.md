This diary file is written by Ching-Yun, Chang E44041034 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.
# 2020-03-22 #
* The introductory lecture introduce some AI success story.
* Learning what is AI nature from website
* making ppt with AI success story. search information from website

# 2020-03-29
* There is still no announcement of what to do this week.
* I know that teacher is working hard on the video.
* I study about how the machine learning idenitify virus in computer security issue.

# 2020-04-5
* learning what exact neural-networks is from youtube.
* found out that a layer is repersent by a matrix.
* understand how the neural-networks read a image and predict the result.
* understand that how different activation function work.
* Read Ch. 5.1-5.3 in Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning.
* I feel it is hard to read Goodfellow article.
# 2020-04-12
* learning tensorflow from youtube
* feel great about learning new tech
# 2020-04-18
* learning Introduction to data preprocessing, model selection, regularization, activation functions, and neural network types, including convolution.
* watch "How Convolutional Neural Networks work" by Brandon Rohrer .
* Read Ch. 9.1-9.3 in Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning.
# 2020-04-27
* learning tensorflow
* learning how to train MNIST
# 2020-05-06
* training MNIST model
* try different way to train model
# 2020-05-13
* Watch How we can build AI to help humans, not hurt us (zh) 
* Watch How I'm fighting bias in algorithms 
* Watch Fake videos of real people
* Watch AI "Stop Button" Problem 
* Watch Stop Button Solution? 
# 2020-05-22
* talking about general perpose AI affect haumanity
# 2020-05-27
* Learnin Nordling Lab - Python and Deep learning tutorial in Colaboratory
# 2020-06-03
* Doing group project
# 2020-06-15
* Doing group project
# 2020-06-26
* present my group project
* taking exam
# 2020-06-30
* doing nothing