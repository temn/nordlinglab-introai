

# 2020-03-21 #

* This week I've read our lecture about the Success Stories of AI.
* I've learnt that there has been some companies that had their success using AI.  
* I was even surprised to know that an AI game opponent in Alpha Go surpassed numerous Elite Human players. 
* It feels scary for me to think that we are as humanity is slowly developing an Artificial Intelligence to surpass Human Inefficiency in different ways. 
* At the same time, it feels amazing to live in this technological era. 
* I am wish COVID-19 will be cured soon. 



# 2020-03-25 #
* On this week's reading about "What is AI?" 
* I've learnt a lot on how we should dsitinguish AI and its subfields 
* By relating AI to any problem solving algorithm can always be misleading when not used in the right manner 
* AI is a method or a discipline that we as human created to do a specific challenge or solve a specific problem. 


# 2020-04-04 #
* I see that thise week's assignment is to practice phyton
* When I was in undergrad I've already learned phython
* I just reviewed the materials, maybe I dont know some things.

# 2020-04-11 # 
* I watched 3Blue1Brown's Youtube video about Neural Network
* I learned the logic behind the convolutions and activation functions
* The Deep Learning ebook from MIT is really good. 
* I liked it because it is discussing maths behind the operations


# 2020-04-18 #
* This week I've learned the basics in programming a simple neural network
* I learnt how to use Keras
* I leanet how to construct and train a simple neural network model using MNIST handwriting dataset.

# 2020-05-03 #
* I've watched the video about the Gradient Descent
* What I've learnt is that, this function is an optimization function used to minimize the cost of the model.
* Gradient descent is a function that finds the convexity, meaning that it is looking for a local optima.
* Next is I have watched the video about Back Propagation.
* This function is used for feed forward neural networks for fine tuning the weights of the network by minimizing the loss or error rate.

# 2020-05-25 #
* Last week we had our first physical class and it was 100% better than online self learning.
* My thoughts about automation:
* Automating any process or job makes human's life easier and better.
* However, it tends to be humanity's habit and obsession to rely on robots to do their jobs and continuosly make it better and better. 
* We are so obsessed in automation that we cannot notice that we are slowly getting replaced by robots, our jobs and our everyday life is getting tied on the things that we built believing it will improve our lives and it does! and we've already gone so far. 
* Where in fact, we are already close to being a cyborg. Nowadays, we always cary our smart gadgets especially mobile phones or either smart watch that can remind us anything, search anything, shop, and even communicate in long distance.
* We are close to automating our selves. Close as we only need to fuse our gadgets to our bodies diereclty connected to our brain.
* Anyway, for my future work plan is to join this crazy automation (kidding aside). I am currently working in a lab that builds satellites, and I plan to implement AI onboard the satellite's  remote sensing payload.
* This will greatly help automating the operations in our satellite. Another tedious job will be replaced by AI :D 



# Article summary #

* For the article presentation, I plan to present tje paper about MovileNet V2
* The goal of this paper is to develop a light weight NN model.
* This model introduced the Inverted Residual Bottleneck layer.
* This layer greatly impacts the effeciency of the network by reducing the number of trainable parameters while just sacrificing a little bit of accuracy.
* The end product of the paper is a model that can be used for Classification, Object Detection, and Semantic Segmentation.


* This week I've read about Chaotic Neural Networks, it is an old paper written on 1989 in Tokyo Denki University.
* Even though it is old, I think it is still relevant in today's advancement of neural network. 
* It discussed the relationship of artificial neural network to the biological chaotic dynamics of a biological neuron.
* They presented a mathematically characterized response of squid giant axons.
* And from that viewpoint, they generalized it as an element of neural netrwoks which they called "Chaotic Neural Network". 