# 2021.03.18 #
*Prof. already uploaded a lot of codes in cloud(https://colab.research.google.com/drive/1uj3GEmGEJbJeaZEnJiUW0aTjtddkSK9o#scrollTo=liiO43RmM9lO). practice basic programming such like if else loop.
*Practice basic variable and functions
*Prepare success AI story for presentation
*Search interest field for future AI coding study, AI application in Stock market will be my goal.

# 2021.03.25 #
*3 bluebrown-but what is a neural network? Youtube.be/airaruwnkk(last visited 2018-05-09)
*deeplarning.mit.edu
*grandmaster level in starcraft II using milti-agent reinforcement learning.  Nature. 575(7782) 350-354 (2019)

# 2021.04.08 #
*Bigger convolutional network + dropout. Finally 99.3% accuracy. Just apply dropout method, it get higher accuracy.
*TensorFlow and deep learning without a phd, part1 google cloud. Martin Gorner
*keras_01_mnist_ipynb. It can train how to reggeized number. Visualize  
*HW:choose one youtube to learn how neural network apply. Why I like it, provide our opinion  

# 2021.04.15 #
*find a learn mode teacher didn't mention
*I learned how hard a machine can initimate like human being with an emotion thinking. For example, my object is to pursue happy through having a lot money.
But the definetion of happy can not implement to machine.
*In today's course I also know the composed of AI system such as perception, an agent and an objective. We need to define the object we want AI system to solve exactly.
*There is one special thing, AI doesn't have the ability to change their objective in a meaningful way only human being can.

# 2021.04.22 #
*Today's lesson taught us the backpropagation of neural networks. An information input in forwardpass and get the feedback in backwrdpass.
*The loss function by chain rule can calculate gradient and update each of the weights in the neurons.
*It can describe like this, new weight = old weight - learning rate* gradient of the loss a function of the old weight.
*I am still curious how reality of CNN work in our daily life, I can know how strong of tensor flow can recognize number but how does AI connect to those algorithm and apply in real world I am still thinking about that.
*Stochastic gradient descenti is the core algorithm of most of deep learning. Prof also ask us why as known the slope of the loss function smoothly but the results of stochastic can't be instead of like jagged shape.
  From my understanding of today's lesson, it is because thru the jagged shape way can take less time to calculate and real close to the bottom point.
*Prof also hope us can search one video online which can describe the stochastic jaggged phenomenon.

#2021.04.29 #
*Next task (homework) : find video talking about : 1. Emsemble 2. You get lower degree freedom layer 3. Fewer iteration don’t have to reach the minimum  
*Neural network: normalization
*Learn the definetion of loss function and the optimization way to model.
*Discuss the different perspective for Dropout, and TA suggest batch normaliztion is better.
*Activation function : https://zh.wikipedia.org/wiki/%E6%BF%80%E6%B4%BB%E5%87%BD%E6%95%B0.
*Neural network type: auto encoder , for very sure important key parameter can use.
*The inception module and transformers.

# 2021.05.06 #
*Learn the transformers model. Self-attention relating different positions of a single sequence in order to compute a representation of the sequence.
*Learn CNN (convolution neural network), it is the most popular for imgae-classification. It only detecs the patterns by a matrix concept. It also has weights and bias.
*Learn the concept of pooling layers, we can choose max pooling or average pooling to be the filter for image we select.
*Convolution layers can be viewed as a sparsely connected network. Its problem will be like a blurry image.
*Learn the idea of discriminator.

# 2021.05.13 #
*Learn Model validation, the result compare predictions with the true classes. The error is an estimation of the generalisation error.
*Learn the concept about overfitting and underfitted. An optimal model can perform well on trainning and test set.
*Learn about activation functions such as like sigmoid, tanh and ReLU.
*Learing rate if to large may end up in random locations and too small will stuck in local minima.

# 2021.05.20 #
*Watch the video of How we can build AI to help humans, not hurt us. It inspire me to realize as a human being can figure out which target is important and benefit to the society spantaneously.
*Watch the video of Fake videos of real people -- and how to spot them. After watching this video, we knew so many informations can be the fake datas. The only way to reconize is to read a lot of books.
*The video of  AI "Stop Button" Problem - Computerphile tell me the imporatance of stop functionfor AI and how it works.

# 2021.05.27 #
*Krishnamoorthy N; 2021 Rice leaf diseases prediction using deep neural networks with transfer learning, Summary slides by Scott Chen N58081052. 
5 view points of this article
*InceptionResNetV2 is a type of CNN model utilized with transfer learning approach for recognizing
diseases in rice leaf images.
*Data augmentation is to increase the trainning data not test dats since the original datasets is not enough.
*The three major attacking diseases of rice plant like leaf blast, bacterial blight and brown spot and healthy class are considered for this research is workable.
*The simple CNN model was fine-tuned using different hyper parameters and achieved the accuracy of 84.75% by running 15 epochs. 
*The parameters of the proposed model is optimized for the classification task and
obtained a good accuracy of 95.67%.



# 2021.06.16 #
* my face test CNN model record
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.preprocessing import image
from tensorflow.keras.optimizers import RMSprop
import tensorflow as tf
import matplotlib.pyplot as plt
import cv2
import os
import numpy as np
from tensorflow.keras.models import load_model
from tensorflow.keras import preprocessing

# img1 = image.load_img('face test/Happy/126822.jpg')
# img1 = image.load_img('C:\\Users\\Lenovo\\Documents\\AI_scott\\face test\\train\\126822.jpg')
img1 = image.load_img(r'C:\Users\Lenovo\Documents\AI_scott\face test\Happy\126822.jpg')

plt.imshow(img1)

img = cv2.imread('face test/Happy/126633.jpg')

# img2 = cv2.imread('face test/Happy/126633.jpg').shape
img.shape

train = ImageDataGenerator(rescale=1/166)
validation = ImageDataGenerator(rescale=1/166)

train_dataset = train.flow_from_directory('face test/train',
                                          target_size=(200,200),
                                          batch_size = 2,
                                          class_mode = 'binary')
validation_dataset = validation.flow_from_directory('face test/train',
                                                    target_size=(200,200),
                                                    batch_size = 2,
                                                    class_mode = 'binary')

temp = (train_dataset[0][0])
temp.shape
# train_dataset[0]

train_dataset.class_indices

train_dataset.classes

model = tf.keras.models.Sequential(
    [
        tf.keras.layers.Conv2D(16,(3,3),activation = 'relu',kernel_initializer='lecun_normal', input_shape = (200,200,3)),
        tf.keras.layers.MaxPool2D(2,2),
        #
        tf.keras.layers.Conv2D(32,(3,3),activation = 'relu',kernel_initializer='lecun_normal',),
        tf.keras.layers.MaxPool2D(2,2),
        
#         tf.keras.layers.Conv2D(64,(3,3),activation = 'relu'),
#         tf.keras.layers.MaxPool2D(2,2),
        
        tf.keras.layers.Flatten(),
        
        tf.keras.layers.Dropout(0.5),
        
        tf.keras.layers.Dense(512,activation = 'relu',kernel_initializer='lecun_normal',),
        tf.keras.layers.Dropout(0.5),
        
        tf.keras.layers.Dense(128,activation = 'relu',kernel_initializer='lecun_normal',),
        tf.keras.layers.Dropout(0.5),
        
        tf.keras.layers.Dense(64,activation = 'relu',kernel_initializer='lecun_normal',),
        tf.keras.layers.Dropout(0.5),
        
        tf.keras.layers.Dense(4,activation = 'relu',kernel_initializer='lecun_normal',),
        tf.keras.layers.Dropout(0.5),
        
        
#         tf.keras.layers.Dropout(0.5),
        tf.keras.layers.Dense(1,activation = 'sigmoid')
    ]
)

model.compile(loss = 'binary_crossentropy',
             optimizer = RMSprop(learning_rate=0.001),
             metrics=['accuracy'])

model_fit = model.fit(train_dataset,
                     steps_per_epoch=1,
                     epochs=50,
                     validation_data=validation_dataset)

dir_path = 'face test/train'

for i in os.listdir(dir_path):
    print(i)
    
    dir_path = 'face test/train'

for root, dirs, files in os.walk(dir_path):
    
    
#     for dir in dirs:
#         print('dir: ', dir)
    
    for file in files:
        
        temp_path_img = os.path.join(root, file)
        
#         print('file: ', file)
#         print('temp_path_img: ', temp_path_img)
        img = image.load_img(temp_path_img)
    
        plt.figure()
        plt.imshow(img)
        plt.show()
# for i in os.li



stdir(dir_path):
#     img = image.load_img(dir_path+'//'+i)
#     plt.imshow(img)

validation_dataset

for temp_data_batch in validation_dataset:
    
#     print(temp_data[0].shape)

#     temp_val_score = model.predict(temp_data_batch[0])

#     print('temp_val_score: ', temp_val_score)

    for temp_data in temp_data_batch[0]:
        
        print(temp_data.shape)
        
        temp_data = np.expand_dims(temp_data, axis=0)
        
        print(temp_data.shape)
        
        temp_val_score = model.predict(temp_data)
        
        print('temp_val_score: ', temp_val_score)
        
        plt.figure()
        plt.imshow(temp_data[0, :, :, :], 'gray')
        plt.title(temp_val_score[0][0])
        plt.show()
        
#         break
#     break


