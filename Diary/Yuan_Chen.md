The daily learning log was created by Yuan_Chen N48091071 in N.C.K.U 2020-3-6 
in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2021-03-04 #

*Had learn basic concept of AI model, include data analyzing, big data, machine learning.

*Had learn deep learning in different purpose and the layer structure that I feel a little bit hard.

*Had learn the AI technology had change the world day by day, and that is happing the auto driver car will make the driver loss job more and more.

*Had ask a queation in class about why we chiose deep learning to be the main title  in this course? and its different between others, like SVM so on in machine learning fields.

# 2021-03-11 #

*Had learn the python to run the code in web in this course, it is very convenience.

*Had learn the code "print" it is amaze, the same with most computer langauage, ex C++, VB, and so on.

*Had learn the intrduction of AI from the material in class show with me, "Basic programming in Python 3".

*Had learn What is AI? chapter1, for auto driver car, image process and so on.

# 2021-03-18 #

*Try to follow the exercise in class to write some code as below:
 a=input("a = ")
 b=input("b = ")
 print(type(a))
 m=int(a)*int(b)
 print("multiplication=",m)
 print("summation=",int(a)+int(b))
 print("sub=",int(a)-int(b))
 print("divsion=",int(a)/int(b))
 
 and the output is:
  a = 1
  b = 2
  <class 'str'>
  multiplication= 2
  summation= 3
  sub= -1
  divsion= 0.5
  
  it work well !!
  
 *Had learn the python code in w3school(http://w3school) for module, numpy, date, math, file and so on,to know how it work.
 *Had learn tensorflow package for structure and try to know how to use such a complex module, but fail. I need practice more.
 *Had write some code to show image file, try to process image pixel as follow:
 
img = cv2.imread("N48091071_RS_110.jpg", 0)
img_orign = cv2.imread("N48091071_RS_110.jpg", 0)
for i in range(0,100):
   for j in range(0,100):
   	img.itemset((1000+i*2,800+j*2),img_orign.item(i+900,j+700))
   	img.itemset((1000+i*2,800+j*2+1),(img_orign.item(i+900,j+700)+img_orign.item(i+900,700+j+1))/2)
   	img.itemset((1000+i*2+1,800+j*2),(img_orign.item(i+900,j+700)+img_orign.item(900+i+1,700+j))/2)
   	img.itemset((1000+i*2+1,800+j*2+1),(img_orign.item(900+i,700+j)+img_orign.item(900+i+1,700+j)+img_orign.item(900+i,700+j+1)+img_orign.item(900+i+1,700+j+1))/4)
    img=cv2.rectangle(img,(700,900),(800,1000),255,1)
    img=cv2.rectangle(img,(800,1000),(1000,1200),255,1)
cv2.imwrite("N48091071_RS_110_result2.jpg",img)
 
 it work well !!
 
 
 # 2021-03-25 #
 *Read the book that professor provided to us:
 "https://www.deeplearningbook.org/contents/part_research.html"
 we can clearly know the different between unsupervised and supervised learning:
  supervised learning need more data to training, unsupervised learning do not need many data.
  The future research many take some parts to focus on unsupervised learning fields, because we usually can't easily to get the data.
  or we can just want to know some concepts for pre-learning before supervised learning, or before we decision to put resource to research.
  It is very importance before we put cost to do some study.

*By the way, I try to use some code run tensorfolow , get fresh test as follow:
I know there are many distributes in python and tensorflow.(reference the org web : https://www.tensorflow.org/tutorials/quickstart/beginner?hl=zh-tw)

import tensorflow as tf
mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10)
])

predictions = model(x_train[:1]).numpy()
predictions

*the output------
array([[ 0.5822236 , -0.50505507, -0.04283711, -0.4105662 , -0.22757468,
         0.62330085, -0.4221993 , -0.33917367,  0.05016939, -0.2516808 ]],
      dtype=float32)

tf.nn.softmax(predictions).numpy()

*the output------
array([[0.18129903, 0.06112183, 0.09703648, 0.06717881, 0.08066861,
        0.18890136, 0.06640184, 0.07215023, 0.10649452, 0.07874725]],
      dtype=float32)
loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
loss_fn(y_train[:1], predictions).numpy()

*output---->
1.6665303


model.compile(optimizer='adam',
              loss=loss_fn,
              metrics=['accuracy'])
model.fit(x_train, y_train, epochs=5)

*output-------
* test result

Epoch 1/5
1875/1875 [==============================] *8s 4ms/step *loss: 0.2958 *accuracy: 0.9134
Epoch 2/5
1875/1875 [==============================] *7s 4ms/step *loss: 0.1450 *accuracy: 0.9574
Epoch 3/5
1875/1875 [==============================] *7s 4ms/step *loss: 0.1070 *accuracy: 0.9676
Epoch 4/5
1875/1875 [==============================] *8s 4ms/step *loss: 0.0876 *accuracy: 0.9732
Epoch 5/5
1875/1875 [==============================] *8s 4ms/step *loss: 0.0734 *accuracy: 0.9771
<tensorflow.python.keras.callbacks.History object at 0x00000215CF923670>


It was work in my computer, but I really did not know the mean of every code, need keep learning.


# 2021-04-08 #

*Learn the machine learning tutorial on youtube
Link:https://youtu.be/Gj0iyo265bc
The video teach me the machine learning use TensorFlow module with build-in algorithm.
How to use the data set from mnist and demo the code in jupter notebook.
The author show picture with the image filter, demo how it work clear.
That Help me understand how to load the datasets, spilt the train and test rate,and how to predict to result and evaluate the performance.
*Had watch the follow train video in www：
https://youtu.be/IHZwWFHWa-w
that remind how the Convolutional Neural Network work,
*but there are somehow the loss and cost function work, so continution the video:
https://youtu.be/E1yyaLRUnLo
that demo how the loss and cost function work in python language.
The the cost function is equal to sumary the total loss, and the loss has different catargeries :ABS mean , square, log entorpy and so on.
The most loss we usually use is log entropy (ex. MAE,MSE,MBE,Cross Entropy Loss, SVMloss)
the link in website show more detial, it is pitty useful in understanding machine learning:
https://towardsdatascience.com/common-loss-functions-in-machine-learning-46af0ffc4d23
*And finally, I read the video about deep learning training material link:
https://youtu.be/O5xeyoRL95U
This is the basics tutoral I think it is more easy for who just learn CNN in begin.
That clearly teach the whole concept in CNN, incluse the convolutional neural work in image classfication, the neurals co-work and the bias, loss, cost and activation function in module design.
Althouth the film is a little bit long ,I think we can take learning after class step by step.
That will help we build more complete module enhance the application in deep learning, because it needs a lot of varivate paramaters while programing.


# 2021-04-13 #

*Today I learn The Keras tutorials:
https://youtu.be/nS1J-2uoKto
This help we know how to improve the accuracy while training our model, the are some easy concepts and demo the code basically, very fanatic and easy to learn.
*I search some paper related to deep learning, about the GPS automation.
*I learn how to evaulate the performance of machine learning: True Positive(TP), False Positive (FP), False Negative (FN) and True Negative (TN)，so called "Confusion Matrix":
*True Positive Rate, TPR = TP /(TP + FN)
*False Negative Rate, FNR=FNR = FN /(TP + FN)
*False Positive Rate , FPR=FPR = FP /(FP + TN)
*True Negative Rate, TNR = TN /(TN + FP)
*Accuracy=(TP+FP)/(TP+FP+TN+FN)
*Precision=TP/(TP+FP)
*Recall=TP/TP+FN
reference materials:
https://www.dataschool.io/simple-guide-to-confusion-matrix-terminology/
https://www.analyticsvidhya.com/blog/2020/04/confusion-matrix-machine-learning/

*This week I try some code on prefessor's Google Colaboratory (Colab) platform to import keras_01_mnist.ipynb, I change add the layer and try to implement the learning rate, change EPOCHS.
*the model as follow:

model = tf.keras.Sequential(
  [
      tf.keras.layers.Input(shape=(28*28,)),
      tf.keras.layers.Dense(128, activation='relu'),
      tf.keras.layers.Dense(128, activation='relu'),
      tf.keras.layers.Dense(56, activation='relu'),
      tf.keras.layers.Dense(10, activation='softmax')
  ])
opt = tf.keras.optimizers.Adam(learning_rate=0.001)
model.compile(optimizer=opt,
              loss='categorical_crossentropy',
              metrics=['accuracy'])

 print model layers
model.summary()

*The accurcacy had great impove from 0.89 to 0.99
*We can fine tuning model to get high performance status.

*I read a paper about remote sensing apply CNNs to imrove the survey in fields, the title is 'Cement Roofing Identification Using Remote Sensing and Convolutional Neural Networks (CNNs)'2020 by Małgorzata Krówczyńska et al. the follow: 
(Open access link of this paper: https://www.mdpi.com/2072-4292/12/3/408/htm)

*Asbestos–cement was wide be used to poor heat conduction in pass decades, but now we find that material would hurt our lung and related to lung cancer for long time issue.  
*This paper use CNN layers information, activation layer was ReLU Max pooling laver (2 x 2 kernel, stride 1) Spatial drop out layer (5%) , the data of 27 by 27 pixels divided to traning for 63.2% and test for 36.8%.
*Asbestos–cement roofing products were classified with the producer’s accuracy of 89% and overall accuracy of 87% and 89%, depending on the image composition used
*The total number of image signatures used for the roof differentiation was 6287, of which 2924 represented the asbestos roofs. The training data set contained 2126 signatures for non-asbestos roofs and 1848 signatures for asbestos roofs, while the test data set contained 1237 signatures for non-asbestos roofs and 1076 signatures for asbestos roofs
*This paper conclusion that use this method CNNs with RGB+CIR can improve the effectiveness of survey and low down the cost.
*Youtube link: https://youtu.be/ZZhyxwOF2bM

# 2021-04-22 #

*Read Back-propagation related materials, follow the link:
*https://machinelearningmastery.com/implement-backpropagation-algorithm-scratch-python/
*https://www.tensorflow.org/model_optimization/guide/pruning/pruning_with_keras
*That materials help me understand the set of CNN base of AI system, incluse how to set learning rate to improve the accuracy, and control Gradient Exploding, Gradient Vanishing.
*I try to use above theory to demo some code, to try how the results work:

model = tf.keras.Sequential(
  [
      tf.keras.layers.Input(shape=(28*28,)),
      tf.keras.layers.Dense(128, activation='relu'),
      tf.keras.layers.Dense(128, activation='relu'),
      tf.keras.layers.Dense(56, activation='relu'),
      tf.keras.layers.Dense(10, activation='softmax')
  ])
opt = tf.keras.optimizers.Adam(learning_rate=0.001)
model.compile(optimizer=opt,
              loss='categorical_crossentropy',
              metrics=['accuracy'])

*print model layers

Model: "sequential_5"

==================================================================

Layer (type)                 Output Shape              Param  
dense_20 (Dense)             (None, 128)               100480    
dense_21 (Dense)             (None, 128)               16512     
dense_22 (Dense)             (None, 56)                7224      
dense_23 (Dense)             (None, 10)                570 

==================================================================

Total params: 124,786

Trainable params: 124,786

Non-trainable params: 0



*training the model epoch=10

steps_per_epoch = 60000//BATCH_SIZE  # 60,000 items in this dataset
history = model.fit(training_images_file,training_labels_file, steps_per_epoch=steps_per_epoch, epochs=EPOCHS,
                 validation_steps=None, callbacks=None)
Steps per epoch:  468
Epoch 1/10
468/468 [==============================] *665s 1s/step *loss: 0.1335 *acc: 0.9644
Epoch 2/10
468/468 [==============================] *646s 1s/step *loss: 0.0012 *acc: 1.0000
Epoch 3/10
468/468 [==============================] *664s 1s/step *loss: 2.2032e-04 *acc: 1.0000
Epoch 4/10
468/468 [==============================] *665s 1s/step *loss: 8.7492e-05 *acc: 1.0000
Epoch 5/10
468/468 [==============================] *666s 1s/step *loss: 4.4832e-05 *acc: 1.0000
Epoch 6/10
468/468 [==============================] *662s 1s/step *loss: 2.6096e-05 *acc: 1.0000
Epoch 7/10
468/468 [==============================] *666s 1s/step *loss: 1.6376e-05 *acc: 1.0000
Epoch 8/10
468/468 [==============================] *673s 1s/step *loss: 1.0815e-05 *acc: 1.0000
Epoch 9/10
468/468 [==============================] *669s 1s/step *loss: 7.4039e-06 *acc: 1.0000
Epoch 10/10
468/468 [==============================] *673s 1s/step *loss: 5.1955e-06 *acc: 1.0000

the result come out a model that accaracy = 1, total train 2 hours

follow will try to evualate the model of test. 


# 2021-04-29 #
This week I follow professor's comment to learn how dropout work, I try to watch video and paper as follow:

*https://www.youtube.com/watch?v=LKSg2vVeI1Q

*https://jmlr.org/papers/volume15/srivastava14a/srivastava14a.pdf

*In the paper above link "Dropout: A Simple Way to Prevent Neural Networks from Overfitting" Nitish Srivastava et, al, Journal of Machine Learning Research 15 (2014) 1929-1958, 
we can learn the detail for the test result between CNN with Dropout and Without Dropout. This paper found dropout technique can prevent units from co-adapting too much. 
But there were still some should take care that it may take 2-3 times longer to train than a standard neural network of the same architecture.
As I know dropout can smooth the gradients of model, so that can prevent overfit. Dropout will stop work while in testing stage, in other words, it only work in training stage. But I still need to read more to really know how it work in detail, this paper is very complicated for me.

*This week I Try to rewiew the deep learning CNN layer with convolution 2D array and pool that professor issue to us. It can lowdown the data counts, meaning we do not need huge data to train our model.

*The layer model set is important that related to our model performance in depand how much data we have that make I more family with the deep learing in CNN.

*I recall the papaer in read 'Cement Roofing Identification Using Remote Sensing and Convolutional Neural Networks (CNNs)', the paper CNN layer structure is similar to class this make me know why the author use pools and 2Dcnn layer, because the data only about one thousand. The data counts are few compare to normal deep learning with CNN.

*It is very critical while our data is not enough, we should come back to design our CNN layer type to overcome our problem, to enhance the perfornance. The concept is useful in the future in my research work.


# 2021-05-13 #

*This week I try to learn underfitting and overfitting, both are needed be care while training our models.
*Training error and generalization errorare both high, this is the underﬁtting.
*As we increase capacity, training errordecreases, but the gap between training and generalization error increases , this is the overﬁtting
*I also review what are Bias and Variance, Bias and variance measure two diﬀerent sources of error in an estimator, Bias: the expected deviation from the true value(target), and Variance on the other hand, provides a measure of the deviation from the expected.
*There are several machline learning : Support Vector Machines, Probabilistic Supervised Learning, logistic regression, Gaussian kernel, nearest neighbor, decision tree, random forest.

# 2021-05-20 #

*This week I try to watch some material as link below: https://youtu.be/HBi-P5j0Kec."Performance measure on multiclass classification [accuracy, f1 score, precision, recall]"
*The video demo two machline learning models, each model has differnet accuracy, one is 54% -A model*and anothers is 87% -B model-. It is very intesting that when conside the F1-score, The model A is more performance the B.
*Why? I am also very curious about the high accuarcy one is not must should the best one. The video tall me that in real world, we should conside some situation that AI machline co-work with human, liking help human to identify Covid-19 in suspect people.
*That is called recall-rate, bacause the high risk while the true covid-19 patient was dectected to health person*False Negative-, that will cause huge damage in our country. The person will keep infecting people if we miss the catch.
*So that the high recall rate model is more useful than low recall rate model in high risk situation. In other words, accuracy is not the only one parameter to be consided.
*I think the concent of F1-scope is vety helpful in machline learning world.

# 2021-05-27 #

*This week I try to use python to run image classify, to learn how the k-mean , random-forest, and so on to learn how to work in python.
*It is very easy to run the code in python, just need one page code, we could run the k-mean well, and evaluate the modle we training.
*the code just as follow(part of):
  
  kmeans = KMeans(n_clusters=  5, random_state = 42).fit(df_island)
  result = kmeans.labels_.reshape(island.shape[0],island.shape[1])
  plt.figure(num=None, figsize=(8, 6), dpi=80)

*I keep to learning random-forest to cassify the image the code just as follow(part of) :
 
 ===========
  classifier = RandomForestClassifier(n_jobs=-1)  
  classifier.fit(training_objects, training_labels)  
  predicted = classifier.predict(objects)  

*Finally, I learn how to evaluate the model by use as follow :

============   
   from sklearn.metrics import classification_report
   report = classification_report(truth[idx], pred[idx], digits=5)
   print(report):

*My training model scope :
              precision    recall  f1-score   support

           1    0.66667   0.50000   0.57143         4
           2    0.66667   1.00000   0.80000         4
           3    0.71429   0.83333   0.76923         6
           4    1.00000   0.33333   0.50000         3
           5    1.00000   0.20000   0.33333         5
           6    0.63636   1.00000   0.77778         7
    accuracy                        0.68966        29
    
    macro avg    0.78066   0.64444   0.62530        29
   
    weighted avg    0.76116   0.68966   0.64525        29
  
 *referance:
*https://earthpy.readthedocs.io/en/latest/gallery_vignettes/plot_raster_stack_crop.html
*https://www.earthdatascience.org/courses/use-data-open-source-python/intro-raster-data-python/raster-data-processing/classify-plot-raster-data-in-python/
*https://www.unioviedo.es/compnum/labs/new/kmeans.html
*https://towardsdatascience.com/object-based-land-cover-classification-with-python-cbe54e9c9e24


# 2021-06-03 #
 
 *This I try hard to training our group model for nmlist. We study a lot parmenter and tips from our trying work.
 *The training model can do "callback" function, that mean we can tell our model some thing to do, in order to come out wandful model.
 *A case to use "ModelCheckpoint" code, this code in keras.callbacks, we can save best weight or model during our train process.
 *Example like this :
  
  *call = ModelCheckpoint('good.h5',
                     monitor = 'val_accuracy',verbose = 1,
                     save_best_only = True,
                     save_weights_only = False,   # save all
                     mode = 'auto',
                     period = 1)
 
 *Above code will save our best model during training while we use large EPOCH (ex. EPOCH = 100).
 *We can not get the best in the final, bout we can "catch" the best while processing training.
 *It is very good to use, and get hi accuracy.
 *Keep walking.
 
# 2021-06-10 #
 
 *This week we had finish the final report. I learn a lot from that class.
 *I learn the UNET CNN, althouth it is very complicated in architecture, but useful. Thanks to my classmate report to us.
 *It can save the proplem from the satellite images, the case is to boundary the feilds of farms, that seem work well.
 *This weekend I also learned a article from website : Deep Learning for Understanding Satellite Imagery: An Experimental Survey.
   follow the link: https://www.frontiersin.org/articles/10.3389/frai.2020.534696/full
 *The article tell us that the CNN had used in image survey well, and there are get more and more importance in the future.
 *The article teach us the Intersection of Union (IoU) between the predicted mask and the ground truth to evluate the performance.
 *This week I get a lot knowledge filled in my brain. I need to make those nutritious thoughts seeded in future.  
  
 *This week I read some paper about the deep-learning apply to remote sensing.
 *one of them use CNN to use a deep Learning method to predict Population from Satellite Imagery, follow link:
 *file:///C:/Users/chuyuachen/Downloads/A_Deep_Learning_Approach_for_Population_Estimation.pdf
 *This paper use VGG-A model to training Satellite Imagery and estimate the area of US Census people density.
 *This is a county level estimates, this model can easy divide high density people area and no people area, but if the area only few people could hard to classfication.
 *The study approach dataset of 2000 and 2010 from the US Census, image size to (74,74,7) for Landsat-7.
 *The result point that the model perform well on the task of projecting county population.
 *This paper conclusion that the future work could on extending our current methodology in several different ways like try different loss function.
 *This paper use VGG-A model to training to estimate the area of US Census people density.

