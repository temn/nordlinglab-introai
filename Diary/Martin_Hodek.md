This diary file is written by Martin Hodek P06088415 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-20 #

* Course had started, it is bit rough to get to know all the online stuff. I have never worked with bitbucked and moodle, partly in Chinese, is also quite puzzling.
* I have watched the intorduction video, read through Nordling Lab web and searched Bitbucket. Slightly overwhelmed. Homework comes next.
* While reading (and creating) AI sucessful stories (first homework), I have come to the conclusion, that if one can provide appropriate data set, on which AI can learn, it can then "quicky" perform various tasks over huge quantities of data, mainly conected to the data recognition. 
* AI can outperform humans in many fields, often due to higher computing capacity and sometimes due to greater "base knowledge", yet this should not be feared, but utilized.
* Still wondering, can AI get out of control, as it is depicted in Sci-fi? In my opinion no.

# 2020-03-27 #

* AI intorduction and definition lesson.
* AI should be defined by autonomous and adaptive behaviour, not needing human guidance and being able to learn from experience.
* Computer science > AI > Machine learning > Deep learning (subfields).
* Robot is a machine, comprised of sensors and actuators.
* "Inteligence" does not stand for consciousness. The AI performs tasks but does not know why.
* Narrow AI is suitable to handle one specific task, general AI should handle any intelectual task (There is no AGI so far).
* Strong AI is self-conscious, weak AI is just program executing some task, not being aware of what it is doing.

# 2020-04-02 #

* Spring break, I did not expect class to happen this week. No materials to study uploaded to bitbucket for this week.
* On Friday (as I was already off Tainan), new message on forum said, that we should follow matterials from previous years.
* There are no materials from previous years for this week. Confusing. 

# 2020-04-12 #

* I am really unsure, how this course will proceed. There are no up-to-date materials and very little guidance on what we are supposed to do. 
* Not to completely lose the track, I went through some of the materials from previous year and searched for Python basics and introduction online.
* I have downloaded Python, Docker and VScode and I am trying to understand these programs.
* Basics tasks in Python are fine, utilizing Docker with VScode is still very problematic and mostly not working. 
* So far, this course is more less complete self study, which is very discouraging. 

# 2020-04-19 #

* On this week, 5th or 6th toppic (may vary due to spring break) should be on the agenda. Obviously, I am bit behind the schedule. 
* First online voluntary Q&A lesson was held this week. It seems, there will never be much of a direct guidance, but we got materials to study. That is appreciated.
* I am still practicing in Python, using VScode editor. Nowhere near being perfect, but by trial and error, I have learned how to do some general coding. Moving on to TensorFlow. 
* Realizing TensorFlow supports Python only up to version 3.7, I had to change my setup and therefore downloaded Anaconda 3.7. 
* By now, virtual enviroment for practicing with Tensorflow within conda is set up. Now it will be necessary to learn more about Tensorflow itself, Neural networks and Modeling.

# 2020-04-23 #

* There was another online Q&A lesson today. Turns out, we were not supposed to instal any software on our computers...
* Neural networks - neuron can be perceived as a function, taking inputs from previous layer neurons, calculating weighted sum of inputs, adding bias and feeding the result to activation function. This function output is then number - probbility. (Probability of this number being an 8)
* Neural networks - "activation" of the neuron is given by weighted inputs (connections) and bias (one bias per neuron).
* Generalization - The ability to perform well on previously unobserved inputs is called generalization. There are training errors and generalization (test) errors. If the neural network has too many degrees of freedom, it will memorize all the training samples - that is bad, causing bad performance on test samples. We have to force the NN to generalize its algorithms - creates beter test results.
* Generalization error - expected error while working with new-unseen data samples. (Test set)
* Capacity - Underfitting - model can not reach sufficiently low error on training set. Overfitting - gap between the training error and test error is too large. (Caused by giving to much degree of freedom and/or too little data.) Both of these can be contorlled by model "Capacity". Machine learning algorithms will generally perform best when their capacity is appropriate for the true complexity of the task they need to perform and the amount of training data they are provided with. Models with insuﬃcient capacity are unable to solve complex tasks. Models with high capacity can solve complextasks, but when their capacity is higher than needed to solve the present task, they may overﬁt.
* Capacity can be imagined as degree of polynom. Its effect is comaparble to plotting data points in EXCEL. :D
* Regularization - Telling the model to prefer one type of solution over another by chenging weights (For example prefering more linear functions - even though model can use high degree polynoms - for plotting. Preferencis are made accordingly to the task model should solve). Regularization is any modiﬁcation we make to alearning algorithm that is intended to reduce its generalization error but not itstraining error.
* Hyperparameters - Controls algorithm's behaviour. Degree of polynom is hyperparameter of capacity.
* Validation set - "part of" training set, used to decide hyperparameters. May cause problems if the complete training set has little samples.
* Number recognition is clasification task. There are many other task nural network can perform. 
* Finally learning some interesting facts, yet the physical class would be preferred anyway.
* Variable equals a degree of freedom in TensorFlow. (For ex. weights and biases) Placeholder is used for insertion of inputs.
* Preffered activation function is these days Relu, it used to be sigmoid. 
* Why does stacking more layers help to increase the acuracy? 

# 2020-04-28 #

* Guideline - DNN intorduction: https://codelabs.developers.google.com/codelabs/cloud-tensorflow-mnist/#3
* DNN training - "Correctness function" may be Cross-entropy loss - taining minimalises cross-enthropy. Using gradient descend, we try to pnverge to the minimal point of cross-enthropy. Problem - local vs global minimum.
* Dense layer: a layer of neurons where each neuron is connected to all the neurons in the previous layer.
* DNN convergence and successful recognition rate can be improved by using - better Optimizer - not to get stucak at saddle pointd (grad=0), using Learning rate decay - slows down learning rate, thus more precise step, using Dropout to reduce degrees of freedom and overfitting. 
* For image recognition, convolutional networks are used - CNN - that do not lineralize the picture into line (=data loss)
* In dense layers, each neuron had its own weights. in convolutional layer a single "patch" of weights slides across the image in both directions (a "convolution").
* With no stride, input data are "reformed" yet the size of input data "cube" is not reduced in horizontal direction. 
* Pooling (using stride) decreas the data size 28x28 to 14x14 (example) and by its nature it redces CNN sensitivity to the position of "exact feature" at the picture - If recognizing cars, I do not mind where in the picture the wheel is ;)
* A pooling function replaces the output of the net at a certain location with asummary statistic of the nearby outputs
* Batch normalization - final trick for CNN - more info: https://codelabs.developers.google.com/codelabs/cloud-tensorflow-mnist/#12
* CNN - input = x, kernel = w, output = feature map. Convilution - flips kernel, cross-correlation - deos not. Otherwise same.  
* Convolution is thus dramatically moreeﬃcient than dense matrix multiplication in terms of the memory requirementsand statistical eﬃciency. - Due to sparse interactions (first layer neuron is not connected to all second layer neurons, only to some), parameter sharing (page 333), equivariant.
* Stochastic gradient descent, improvement to regular gradient descend - training on mini batches.
* Perceptorn neurons - 0 or 1 output, Sigmoid neurons - 0 to 1 output
* Feedforward network - no loops, recurrent network - looped

# 2020-05-07 #

* Online lesson did not take place.
* No materials nor guidelines for group project (even though they were proposed for today class at latest) were uploaded.
* 22:42 we got moodle notification, that the guidelines were submited. WHY THAT LATE?? 
* Why the guidelines changed and the Neural Network Task is now different from what was previousy suggested?? 
* The course is complete mess and I am one of many with this oppinion. 

# 2020-05-14 #

* First real-offline class. Quite nice, but now we are no more dealing with the problematic DNN topics, which was the time the proper teaching was needed the most.
* Dealing with Ethics and the danger of algorithm bias - propper (non-biased, non discriminating) learning sets are necessary.
* AI fakeing the information (video, pictures, faces) problem discussed.
* AI Stop button - interesting open problem. GAI must not be motivated nor penalised for button pressing, yet it must value the button, not to cut it from the "utility function".

# 2020-05-21 #

* Edited "AI sucessful stories" presentation.
* Found article on topic of supervised DNN training - Webly Supervised Learning of Convolutional Networks.
* Read through, summarized article, prepaired the presentation slides.
* Getting ready for the CNN for digits (and letters) recognition.
* Automation and the future of work thoughts - Regarding the DNN utilization, I believe, that time consuming overall data processing will be speed up and imporved by DNN, which may be considered as the main highlight in public sector. Aside from that, DNN will surely be designed to perform very specific, up to scientific tasks, saving time and money, while also improving the performance.

# 2020-05-29 #

* Second "offline" lesson, focused on automatization. We did not manage to go through the whole topic, so there will be another next week.
* Overall it can be said, that we are now in the Third Industrial Revolution phase, and the professor believes that AI will save the world.
* The professor seem to be very tech-optimistic yet some of the presented data are clearly misleading and missing the context.
* I am looking forward to the next lesson, as there the potential of AI should be discussed.
* Aside from this, I am working on my CNN. It is not that hard but since I have very little coding experience, it is mostly trial and error based while the only part I do understand is the model architecture.

# 2020-06-04 #

* Second part of the previous lesson, now focussed on AI affecting our jobs. 
* Presented predictions are impressive, however I do not believe, that AI will have such effect broadly.
* Aside form the lesson, I have worked on CNN to polish it and prepare it for presentation.

# 2020-06-11 #

* Another offline lesson, this time focussed on presenting "Article summary". The idea behind is probably good, but as we all have different backgroun, it was very hard for me, to grasp at least basic concepts from some other people presentations, as their knowledge was probably much greather and focussed on more complex problems of AI.
* Rest of the week, I have spent tweaking and tuning up my CNN "group project" which now I am quite proud of. There is still some work to do, to polish the layout and the code, yet the architecture and all the working parts are already done.
* As I was searching for EMNIST dataset, I have found interesting, that "Balanced" dataset contains numbers from 0 to 9, all the capital letters of alphabet, however ony some of lower case letters.
* Another interesting think to me was, that the AI is able to reproduce its "decision making" process, while labeling numbers. (Meaning, that aside from the label, it is possible to look up "how sure" is the CNN regarding the label.
* As there is a final exem next time, it is time to revise what we have gone through in past months.

# 2020-06-18 #
* Last lesson, devoted to final examination and CNN presentations. 
* The exam was not that hard, yet some of the questions vere tricky and unexpected. Once I am done with rest of my schoolwork, I will try to look up what should have been correct answers.
* Regarding the CNN presentations, many people achived very nice results, yet mostly by employing basic CNN architectures. From a learning perspective, there was not much of new information, however it served well to revise current knowledge.
* Last presentations were presented in rush, as there was not enough time. That was a pitty.

# 2020-06-25 #
* Class is probably ended by now and we are waiting for a results. I hope they will be posted soon.
* It was nice excursion to DNN/CNN world, however the online classes suffered from some difficulties. 
* I am very unsure, whether this diary entry is still counted, yet as I have missed alreda two, I am not risking it again. 

# Article summary #

* [Webly Supervised Learning of Convolutional Networks](http://openaccess.thecvf.com/content_iccv_2015/html/Chen_Webly_Supervised_Learning_ICCV_2015_paper.html)
* Recently, supervized learning is highly limited and dependent on the training set size and non-biased samples and labeling. To solve this problem, aproach utilizig web-available pictures (supervision comes from picture related text/name/tag) is intorduced.
* Curriculum learning approach is utilized, starting with simple and clear images from Google, while the CNN is then fine-tuned on more realistic and complex images obtained from Flickr.
* (Curriculum learning - a machine learning technique inspired by the way humans acquire knowledge and skills: by mastering simple concepts first, and progressing through information with increasing difficulty to grasp more complex topics.)
* As a result, quite noise-robust CNN is generated, whose performance matches and even outperforms ImageNet (labeled pictures database) trained CNNs (2015).
* It is proven, that the web data (available in huge quantities) can be utilized for DNN training even despite its noisiness. 

