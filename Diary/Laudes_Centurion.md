# 2020-03-20 What is AI, Deep Learning, and Machine Learning?
# ARTIFICIAL INTELLIGENCE
## I. HOW SHOULD WE DEFINE AI.
#### A. AI DEFINITION
    AI can have two definitions: 
       1.  Artificial life-forms that can surpass human intelligence.
       2.  Almost any data processing technology
#### B. AI APPLICATIONS
        1. Self-driving cars. 
            -Search and planning to find the most convenient route from A to B
            -Computer vision to identify obstacles
            -Decision making under uncertainty to cope with the complex and dynamic environment.
            
            The same technologies are also used in other autonomous systems such as delivery robots, flying drones, and autonomous ships.
        2. Content recommendation. (Advertaises, enterteiments, etc...)
            -Personalizaded content.
                Example: 
                    Movie recommendation on Netflix. 
                    Advertaisment on Facebook.
        3. Image and video Processing.
            -Face recognition.
                Example:
                    Automatic tagging on social media.
                    Passport control.
            -Generate or alter visual content
                    Pixar animations where the animated characters replicate gestures made by real human actors.
#### C. AI MISUNDERSTANTINGS
    Reasos of why people tend to confuss or do not undersstand what AI means.
        1. No officially agree definition.
        2. Legacy of Science Fiction.
        3. Difficulty in understanding AI.
        
#### D. AI PROPERTIES
        1. Autonomy
            The ability to perform tasks in complex environments without constant guidance by a user.
        2. Adaptivity
            The ability to improve performance by learning from experience.

* AI as a discipline: It is a collection of concepts, problems, and methods for solving them.

## II. RELATED FIELDS
#### E. AI RELATED TOPICS
        1. Machine Learning:
               -Systems that improve their performance in a given task with more and more experience or data.
               -Central Branch of AI.
        2. Data Science.
            -Covers several subdisciplines as:
                -Machine learning
                -Statistics
                -Computer science (algorithms, data storage, and web application development)
        3. Deep Learning:
            The “depth” of deep learning refers to the complexity of a mathematical model
        4. Robotics::
                Building and programming robots that can operate in complex, real-world scenarios.
        
* Computer Science -> AI -> Machine Learning -> Deep Learning.


## III. PHILOSOPHY OF AI
        -The definition of intelligence, natural or artificial, and consciousness appears to be extremely evasive
        and leads to apparently never-ending discourse. 
        -Book about it: The Mind’s I by Hofstadter and Dennett.
        
* Narrow AI refers to AI that handles one task
* Artificial General Intelligence (AGI) refers to a machine that can handle any intellectual task. 
* Strong AI would amount to a “mind” that is genuinely intelligent and self-conscious.
* Weak AI is what we actually have, namely systems that exhibit intelligent behaviors despite being “mere“ computers.


# 2020-03-29
1. We do not have homework this week.
2. I read more about AI in the link recommended by the prof.

# 2020-04-05
1. We do not have homework this week.
2. I will spare some time to learn more about Python this week.
        
# 2020-04-12
1. Read about Python. 

# 2020-04-19 Introduction to Python (Getting help and comments, Printing, Conditional statements and loops) 
1. Read the presentation about the definitions of Machine Learning, Deep Learning, Ai, etc...
2. Reviewed those concepts.
3. Started programming in Python. Got excited :D

# 2020-04-26 Introduction to Python (Variables and functions, Reading and writing files, Error handling)
1. In this class the professor guided the students and answered class-related questions.
2. Did all the python excercises.
3. Watched the videos suggested by professor.

# 2020 - 05 -03 Introduction to TensorFlow
1. In class, the teacher explained about the homework and Tensorflow.
2. Corrected the Tensorflow program.
3. I chosed to do the final project alone.

# 2020 - 05 - 07: Introduction to Neural Networks and Modelling 
## 5.1 Learning Algorithms
*A machine learning algorithm is an algorithm that is able to learn from data.
“A computer program is said to learn from experience E with respect to someclass 
of tasks T and performance measure P, if its performance at tasks inT, asmeasured byP, improves with experienceE.” ( Mitchell (1997))
## 5.1.1 The Task T
*Machine learning enables us to tackle tasks that are too diﬃcult to solve with ﬁxed programs written and designed by human beings. 
*

# 2020 - 05 - 14: Gradient descent, Backpropagation, and loss functions
## 5.9 Stochastic Gradient Descent (SGD)
            Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning
    Problem in Machine Learning: 
        * Large training sets are required for good generalization
        * Gradient Descent requieres computing.
        * The training set sizes grows to billions of examples.
        * The time to take a single gradient step is extremately long.
    ### Stochastic Gradient Descent (SGD)
        * Is the standard learning algorithm for neural networks.
        * Is an algorithm for powering deep learning.
        * Its insight is that the gradient is an expectation.
        * This expectation can be estimated by using a small amount of samples.
        *In each step of the algorithm we can sample a minibatch of samples.
        The minibach is a group of small number (ranging from one to a few hundreds) of samples. It does not grow in size when the training set grows.
The optimization algorithm may not be guaranteed to arrive at even alocal minimum in a reasonable amount of time, but it often ﬁnds a very low valueof the cost function quickly enough to be useful.
Prior to the advent of deep learning, the main way to learn nonlinear modelswas to use the kernel trick in combination with a linear model.

## "Using neural nets to recognize handwritten digits" in Neural Networks and Deep Learning by Michael Nielsen.
* Human brain can easily recognize handwritten numbers, computer programs, do not.
* Nueral network take a sample set (training set)and develop a system which can learn from the training set.
* The network undrstand easily when the training set is big.
* Handwriting recognition is an excellent prototype problem for learning about neural networks in general.

 ### Presentron:
 * Is an algorithm for supervised learning of binary classifiers.
 * Type of artificial neuron.
 * Is a 'device' that makes decisions by weighing up evidence.
 * A perceptron takes several binary inputs, x1,x2,…, and produces a single binary output.
 * Weight: real numbers expressing the importance of the respective inputs to the output.
 * By varying the weights and the threshold, we can get different models of decision-making.
 * The neuron's output, 0 or 1, is determined by whether the weighted sum ∑jwjxj is less than or greater than some threshold value.
 * A many-layer network of perceptrons can engage in sophisticated decision making.
 * Uses weigthed layers: In the second layer it can make a decision at a more complex and more abstract level than perceptrons in the first layer. And even more complex decisions can be made by the perceptron in the third layer.
 

# 2020 - 05 - 21: Ethics and the danger of algorithm bias
1. First presential class.
2. Professor explained the impllications of a General AI.
3. Discussing and listening to my classmates made me got a broader and better understanding of AI.

# 2020 - 05 - 28: Application of Deep Learning (group project)
1. I am doing the group proyect alone.
2. Used the code provided by the professor.
3. Read to understand the code.
4. Installed tensorflow, jupyter, mnist and mlxtend.
5. Had error in jupyter notebook.
6. Fixed error.
7. Succesfully trained a few times.
8. Accuracy 93%
9. Next step: improve accuracy.

# 2020 - 06 - 3: Automation and the future of work
#### My dream job
My dream job is one where I can help people making things. I would like to make leg prosthetics or stents. I would like them to be world wild delivery and to be cheap so a lot of people can have access to it. 

Work security 
Entrepreneur

Benefits
Retirement

#### Thoughts about atomatization.
Today's lecture gave me a clear idea of the jobs of the future: All those that embrace automatization. 
Why should we change to automatization? 
Below I point some key advantages: 
1. Higher production rates 
2. Increased productivity 
3. More efficient use of materials. 
4. Better product quality. 
5. Improved safety. 
6. Shorter workweeks for labor [LOHAS in economy (https://en.wikipedia.org/wiki/LOHAS)]
7. Reduced factory lead times.

Today's class touched every question that I have been having about automatization. To cite some: 
1. How long will it take to change to automatization completely? 
2. How will the transition between the life we have now and the one that automatization will bring be? 
3. How will automatization affect people's lives? Etc...

And now I have these two: 
1. How will the worldwide population react when the first autonomous car kills a human? 
2. How will the COVID-19 crisis hit the actual prediction of automatization/technology predictions?

The highlight of this class for me is: 
1. Many jobs are going to disappear but it will take some time (one decade or more) for people to change jobs completely. 
2. Aim or jobs where AI and automatization are applied. Can this job be automatized? Yes? CHANGE! No? No really? GO AHEAD! 
3. Genetics is the new future.

# 2020-06-14
We did not have presential class this week.
My goal for this week was improve my code for the final project but I did not do it.
Looking forward to this week class (the last one before exam).
My goal for this week is prepare the final presentation, improve code, prepare for exam.
I completed the class survey.

# 2020-06-24
1. Tomorrow is the last class of this course.
2. We will have an exam and present our final project.
3. I improved the accuracy of my final project. Now, it is 98%.
4. I did two more projects: A. Alphabet training and recognition. B. Recognition of a number on an image. 

# 2020-07-01
1. Had final exam
2. Had final presentation
3. Submitted group project (MNIST and EMNIST database)
4. We did not have class this week.
