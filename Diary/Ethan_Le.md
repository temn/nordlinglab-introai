This diary file is written by Ethan Le in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2021-03-04 #

* This is the diary for the first two lectures of the course, so far so good I think. 
* Seems like that I got a lot of thing to learn from simple stuff like version control via Git or Python programming to AI, DL, ML...
* Should take a look at the Markdown syntax stuff, looks similar like Latex somehow. 
* Although sounds kinda fancy (the AI stuff), the model you got is only as good as your data. 
* Should revise a little bit about basic statistic. 
* Have good feeling, feel excited to learn one thing or two. 

# 2021-03-12 #
* This has been a super busy week with all of the proposal writing. When you write proposals more than you write codes, maybe it's not a good sign. 
* However, proposal is money and job, so...just have to go with it then. 
* Gonna find time to read the first chapter of the book "Intro to AI". 
* Take a look at the TensorFlow tutorial. 
* Should spend more time on the course material and outline. 
* Just finish another great book "Calling Bullshit". Trying to find the next book to read, still want to read more and more. 

# 2021-03-19 #
* Finish off with all the proposal writing bullshit. Can get back to work now. 
* Found some great books to dive into: Walter Isaccson' biograhpy series, and a book about stem cell. Can wait to start with those books. 
* This is the  final semester of the my Ph.D. candidature, feel no motivation at all, just one to be submerged in books. 
* One of the new year resolution is to improve my coding. Can be a second career after graduation. 
* Still try to spend enough time for running and weight training. Really hope that I can got that 100kg bench before the defense. 
* Code something fun------Run some long distance-----and Lift some heavy shit. 

# 2021-03-26 #
* Decided to start two books at the same time (never tried before). The first one is the Codebreaker, the second one is the Flesh Made New, "And Then There Were None" in the weekend maybe.
* Reading the "Deep Learning with Python" by the guy created Keras, seems easier to grasp than the Deep Learning from MIT, less mathematical notations. 
* Started to feel worried about future career after graduation. Wanna go somewhere else to learn something new. 
* Belly seems to get bigger, need to control my eating more closely. 

# 2021-04-02 #
* Paper got rejected, need to find a new journal for resubmission. 
* Finish the "And then there were none", reading the "Flesh Made New", pretty interesting book. Highly recommended.
* Keep reading the "Deep Learning with Python" and getting to be familiar with Keras. Doing some Python advanced drills. 
* So far so good. However, just realized that I got too many defenses to do for my graduation. 

# 2021-04-11 #
* Finished a lot of examples of using DL to do binary and multiclass classification, as well as regression. 
* Working on the Convolutional Neural Network to classify cats and dogs images data from Kaggle. Pretty fun, have to say. 
* Reached chapter 6 of the "Flesh Made New". Very good book to read about cells and the field of regenerative medicine. 
* I personally think that the materials from Martin Gorner are insightful but surely not for beginners. 
* My squat and deadlift are getting better. Trying conventional deadlift recently and feeling pretty strong. 

# 2021-04-25 #
* Feeling the quality of the course and the lecture is slowly goes downhill. The lecturer seems like he does not invest his time for the course. Mostly presents videos from Youtube and last course was terrible, could not hear a single word from the videos. 
* Quite busy with the thesis writing and manuscript writing recently. Got the write code, thesis and paper at the same time in the storage without AC. 
* Hopefully I can get a job after graduation, quite unclear about future or the direction ahead. 
* All of my lifts has been stuck for some times. Need to try harder. 

# 2021-05-17 #
* It has been a long time I haven't written anything in this diary. Crazy time with the thesis, the simulation, and the paper. 
* Just finished the first draft of my last paper as a Ph.D. candidate. Fastest manuscript writing so far.
* Gym will be closed for two weeks (may be more) due to the virus, seems like everything is getting worse pretty fast. Hope everything is gonna be alright. 
* Ironically, when I have the time to come back to course, the course is online. 
* So...only running and some calisthenics for the next two weeks. Poor my chicken legs. 