This diary file is written by Tran Quang Huy N16097122 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2021-03-04 #

* I learned different concepts of Statistics, Data Science, Data Mining, System Identification, Artificial Intelligence, Machine Learning, Deep Learning, and their relationships.
* I was introduced digit recognition case study.
* I was introduced Git, and the importance of Version Control Systems.
* I knew some applications of Deep Learning in our life everyday.

# 2021-03-11 #

* I knew how to print text with Python
* I learned why we need to put comments and how to find the information about functions in Python using help or ?
* I learned conditional statements and loops

# 2021-03-18 #

* I knew how to install TensorFlow and use Jupyter Notebook
* I knew that we don't need to declare data type in Python
* I knew about function in Python
* I knew about class and inheritance in Python

# 2021-03-25 #

* I learned how to read, write, append file in Python
* I learned what is neural network, input, output, hidden layers
* I was introduced reinforcement learning, supervised learning and unsupervised learning.

# 2021-04-08 #

* I can train and run simple neural network model for recognizing handwritten digit using MNIST data set.
* I knew that using ReLU as activation function results in better performance than using sigmoid function.
* I knew the benefit of decay learning rate.
* I learned disadvantages of overfitting.
* Convolutional neural network can keep the shape information and provide better performance.

# 2021-04-15 #

* Classification: specify which of k categories some input belongs to. Example: object recognition
* Regression: Predict a numerical value given some input.
* Transcription: observe unstructured representation of some kind of data. Example: optical character recognition, speech recognition.
* Performance measurement in classification is usually accuracy.
* Unsupervised learning: is learning with unlabeled data.
* Supervised learning work with labeled data.
* Underfitting occurs when the model is not able to obtain a sufficiently low error value on the training set.
* Overfitting occurs when the gap between the training error and test error is too large.
* A model's capacity is its ability to fit a wide variety of functions.
* No free lunch theorem: averaged over all possible data-generating distributions, every classification algorithm has the same error rate when classifying previously unobserved points.
* The goal of machine learning research is not to seek an absolute best learning algorithm. Instead, our goal is tounderstand what kinds of distributions are relevant to the “real world” that an AIagent experiences.

# 2021-04-22 #

* Stochastic gradient descent (SGD) is an extension of the gradient descent algorithm.
* On each step of the algorithm, we can sample a minibatch of examples drawn uniformly from the training set.
* Back propagation is an algorithm that calculates the gradient of the error function with respect to the neural network's weights based on chain rule.
* Using many different batch will help us reduce computational cost, and also avoid local minimum.

# 2021-04-29 #

* Dropout is a simple way to prevent overfitting. It deletes nodes randomly after each step in training process.
* Dropout helps neural networks increase robustness, because it modify the structure of the networks.
* Dropout reduce the number of nodes and edges, so the network will have less freedom to account the noise in the training set.
* There are many types of activation functions in neural network: ReLU, sigmoid, tanh, etc.
* There are many types of loss functions: cross entropy, hinger, huber, Kullback-Leibler, Mean Squared Error...

# 2021-05-06 #

* Inception modules are used to allow for more efficient computation and deeper Networks through a dimensionality reduction with stacked 1×1 convolutions
* Convolutional neural network is applied to analyze visual imagery, it has smaller size than fully connected neural network.
* Kernel (filter) slides across the input features and build feature map. Padding is added around the image to preserve the size.
* Pooling reduce the dimension. Stride is the step the kernel take when slide in the input.
* Teacher-student strategy: there is another very good network that guide the smaller network to pay attention at important data.

# 2021-05-13 #

* Cross-validation: every sample of the data is used for both training and testing in different folds.
* The two central challenge in machine learning is overfitting and underfitting
* Data augmentation helps avoid overfitting when samples are scarce by generating more samples from existing ones by random transformation.
* Bias: measures the expected deviation from the true value of the function.
* Variance: measures the deviation from expected estimator value that any particular sampling of the data is likely to cause.
* Hyperparameter are chosen before the model is trained. Some examples of hyperparameter are layers, activation function, learning rate, batch size, epochs

# 2021-05-20 #

* Biases reflect limited viewpoint to a single dataset and human biases found in the data, such as prejudice and stereotyping.
* Algorithmic bias results in unfairness, it can also lead to exclusionary experiences and discriminary practices.
* Examples of algorithmic bias: The machine considers a burned house's image as a positive scene, a facial recognition software can not detect the face of the black woman, In the US, police departments use facial recognition software in their crime fighting arsenal. It is very dangerous if the face labeling is not accurate.
* Avoid algorithmic bias: create teams with diverse individuals to check each other's blind spot. Make social change an priority.
* Deep fake has many benefit if we use it carefully, but it is also very destructive if we use it to misrepresent other people.
* Stop button problem is a challenge in the development of artificial general intelligence, how to make it corrigible. For example, a robot is going to take a cup of tea, but there is a baby in the path to the cup, how to prevent it hurt the baby. How to set the reward to stop button and main goals?

# 2021-06-03 #

* Planetary boundaries: climate change, novel entities, stratospheric ozone depletion, atmosphere aerosol loading, ocean acidification, biogeochemical flows, freshwater use, land-system change.
* Engineers developing or deploying automation technology have a bright future with plenty of work.
* Artificial intelligence, machine learning, automatic control, robotics are the fields driving the development.

# Article summary #

* [Javier Rodriguez-Puigvert et al. 2015 Bayesian Deep Networks for Supervised Single-View Depth Learning](https://arxiv.org/abs/2104.14202), [Summary slides by Tran Quang Huy N16097122](https://drive.google.com/file/d/1qmE-XIlh4a3PF9omHwikMzHNLF3PM2-_/view?usp=sharing), [YouTube video by Tran Quang Huy N16097122](https://www.youtube.com/watch?v=pOmuYP1Llc8).
* Bayesian Deep Networks for Supervised Single-View Depth Learning
* In this paper, they evaluated mc dropout and deep ensembles as scalable Bayesian approaches to uncertainty quantification for single-view supervised depth learning.
* They have demonstrated that MC dropout in the encoder outperforms the current approach of using it in the decoder.
* In their experiments, depth estimation has a better representation when they use mc dropout on the encoder, but deep ensembles are the slightly better option to represent uncertainty.
* They also demonstrated the application of uncertainty to pseudo-RGBD ICP with the result that relative transformation can be improved by excluding the points with higher uncertainties.
* For future investigation into this topic, they need to address the overconfidence that they observed in their depth prediction networks and research a better representation of uncertainty.