# 2020-03-23 #

* I had trouble using bitbucket and was confused. I'm catching up with the rest now.
* The introduction video's description box made me interested in this course because I have have no knowledge of programming, statistics, or machine learning.

Lecture 1

* It's amazing how lip reading is almost 100% recognizable with an AI.
* It's also terrifying how AIs outperform humans in many aspects.
* I now know in depth why Tesla is gaining so much popularity.
* Waymo is new for me. 
* I'd like to learn about the actual codes and algorithms that make an AI .

Lecture 2

* AIs don't seem too terrifying after realizing that it's much younger than the evolution of human beings and that we decide/invent how intelligent it can be.
* I won't use the term 'AIs' from now on. 
* Computer science - AI - machine learning - deep learning
* I should watch the movie "Imitation game" about Alan Turing.
* I wonder why there has been no progress made in AGI.

# 2020-03-29 #

Lecture 3

* Can't find any material to read for this week.
* I wish the professor and the TA would be more interactive on Moodle.

# 2020-04-05 #

Lecture 4

* I did some reading from the 2019 references “Automate the Boring Stuff with Python” and “Learn Python the Hard Way”.
* Tried to install the latest version of Python, but somehow it wouldn't install on my laptop.
* Once I download it, I will immediately start practicing writing codes since I find it extremely interesting. 
* Watched the 3blue1brown youtube video on neural networks. (awesome channel!)
* Had to watch the video several times and pause midway to start to grasp the idea. 
* Let's just say our brains are smarter than us. 

# 2020-04-12 #

Lecture 5

* The ability to perform tasks - learning.

Task T:

* Classification task: input - image, output - numeric code of the object on the image, i.e. facebook automatic tagging.
* Classification with missing inputs: set of functions is needed; probability distribution is used; common in medical diagnosis.
* Regression: prediction.
* Transcription: optical character recognition, speech recignition; Google street view.
* Machine translation: language translators.
* Structured output: a more complex system of transcription and translation.
* Anomaly detection: finding the odd one in a group; credit card fraud.
* Synthesis and sampling: generate new examples that are similar to the input.
* Imputation of missing values: find missing values.
* Denoising: find the error.
* Density estimation: used to solve other tasks.

Performance Measure P:

* P is specific to T
* How to choose the most suitable performance measure?

Experience E:

* Unsupervised learning algorithms experience a random vector x of examples.
* Supervised learning algorithms experience a vector x of examples and associated vector y; follows a label or a target. 

# 2020-04-19 #

Lecture 6

* I've never heard of Colaboratory before. Very useful.
* I watched the "TensorFlow and Deep Learning without a Phd" video. 
* It's still very hard for me to learn how to do write a code.
* I hope we will have an actual online class next week, where the specifics of writing a code is explained. 

# 2020-04-26 #

Lecture 7

Some points I got from "How Convolutional Neural Networks work" by Brandon Rohrer: 

* Repeated action of filtering, i.e. trying every possible match is convolution; we get a stack of images.
* We end up with a smaller but similar pattern through pooling; not as sensitive to position of the values.
* Deep stacking: stack convolution, reLU, pooling layers as many times as needed and get a more filtered image.
* Fully connected layer: the final layer; image or symbol is recognized by the stronger value

From the book "Deep Learning":

* Using convolution seems easier, faster and simpler than using matrix operation.

# 2020-05-03 #

Lecture 8

* I read "Using neural nets to recognize handwritten digits" in Neural Networks and Deep Learning by Michael Nielsen.
* Perceptrons take several binary inputs and produce a single binary output; can weigh different kinds of evidence to make a decision.
* A small change in the weights or bias of any single perceptron in the network can sometimes cause the output of that perceptron to completely flip.
* Sigmoid neurons prevent a big change in the output.
* Hidden layers - not an input or an output.

# 2020-05-10 #

Lecture 9 

* I watched 'How we can build AI to help humans, not hurt us'. We should educate ourselves more about the capabilities of AI and discuss openly to determine the best outcome.
* I watched 'How I'm fighting bias algorithms'. Who codes matters, how we code matters, why we code matters.
* I watched 'Fake videos of real people-and how to stop them'. 'Reality defender' is a must at this rate of computer development. 
* I watched 'AI "Stop Button" problem - Computerphile'. A very thought provoking video. I'm interested to know whether a perfect solution has been proposed yet.
* I watched 'Stop Button Solution? - Computerphile'. Very well articulated, but a bit unsettling.

# 2020-05-17 #

Lecture 10

* We don't have any materials scheduled for this week.
* I have contacted my group members, but we haven't started to work on the project yet. 
* I don't get why we have to switch to physical classes. 

# 2020-05-24 #

Lecture 11

* We had our first physical lecture. I still think it's pointless.
* I wish the professor would explain more about the concepts and coding that's required in the group project.

# 2020-05-31 #

Lecture 12

* Our group has finished the coding part for the project.
My thoughts about automation and my future work plans
* Automation leads to reduced production time and less human error. 
* Automation has led to increased unemployment. 
* However, it is a necessity for companies to stay competitive.
* It drove people to pursue science degrees more. 
* I don't have any specific future plans at this moment.

# 2020-06-07 #

Lecture 13 

* I've been working on the group project.

# 2020-06-15 #

14

* Completely forgot to update my diary due to an influx of final exams and projects.
* Our group will meet tomorrow and record the video. 
* I'm glad that I've somewhat learned what deep learning is. 

# 2020-06-21 #

15

* Here's what I've learned from working on the group project.
* We built a convolutional neural network that has a bunch of neurons as input data, which are then put through hidden convolutional layers to generate an output. 
* Numpy stands for Numerical Python, and it is a python library used for working with arrays and it has functions of mathematical calculations.
* Keras is tensorflow’s high level API for building and training deep learning models, it’s basically a library with a bunch of functions to build models fast. 
* Sequential model has exactly one input tensor and one output tensor. And it uses layers in sequence to process data and get a result. 
* Keras layers are premade layers for a sequential learning model. 
* A dense layer consists of simple neurons.
* The layer flatten is to make our images flat, in other words turn a 2d matrix into a 1d vector.
* A convolution layer extracts features from an image by scanning it with a filter of, say 4 by 4 pixels. For each 4 by 4 pixel region within the image, the convolution operation computes the dot products between the values of the image pixels and the weights defined in the filter.
* The 2D in 2D Convolution refers to the movement of the filter, which moves across the image in 2 dimensions.
* For machine learning models, we need a set of data for training and a set of data to validate the training. (train data, train labels) are for training and (test data, test labels) are for testing the accuracy of the model.
* Accuracy and validtaion accuracy values must be close to each other.

