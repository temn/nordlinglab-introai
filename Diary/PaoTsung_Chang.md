This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown. Note that you should write the diaries for all weeks in the same file.

This diary file is written by PaoTsung_Chang N66094409 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2021-03-04 #

* The statistic is really foundamental tool to learn deep learning.
* ML,DL,data science,data mining and system identification are different field,but they all have statistic.
* AI is already in our life,such like facebook,cellphone typing....I just didn't realize it.
* Is AI always meaning to automation?

# 2021-03-11 #

* This is first time use python and it's a Intuitive language.
* Print "Hello World" is first comment and meaning to check it can run without problem.
* use "?" can do the help, show the notice to read.
* I download the anaconda in my laptop so i can coding in local.

# 2021-03-18 #

* Today teaching so much comment, it need some time to figure out.
* if comment is so useful, and it is easily to read.
* I still doesn't really understand the classes and inheritance part,there are so complex.
* use jupyter notebook in local is more convenient.

# 2021-03-25 #

* it's the first class to learn deep learning, i think the main concept is to classify.
* All the calculation in hidden layer is base on probability.
* Using sigmoid function is a way to let output be zero to one.
* Maybe the hard task is to define the function of weight and bias.
* Supervised learning is expensive, so using unsupervised learning is more common. 

# 2021-04-08#

* Reading martin_gorner's slide. there are too many concept need to understand,like 'overfitting','learning rate decay'.'softmax','cross entropy'...and so on.
* I think replace 'softmax' to 'cross entropy' is also available in write-up,but i can't refine and recode it well.
* Change the "steps_per_epoch".i reduce the value to "20000//BATCH_SIZE", the loss and accuracy become more stable.Besides,it complete more fastly.
* Although 90% seems perform well, when have large enough data still have many mistakes.
* I think sigmoid function is better than ReLU fumction, because it let output in range 0~1. more easy to read!

# 2021-04-15#

* The objection for human can always change and shift quicly, but machine not!
* One of the big difference between human and machine is human have feeling.
* Supervise-learning need to define label,it's really need to spend much time.
* There are many different method,each method has different solution. 

# 2021-04-22#

* For backpropagation,chain rule is important concept.
* Gradient descent is a method to find out the best solution,but it need some stochastic concept
* Chooseing learning rate is a big issue.if too large might miss global minima,if too small might spend more time
* RMSProp seems like a method to have a better learning rate, it is time dependent.
* How to define the function parameter ? It seems need more experience.

# 2021-04-29#

* There are many ways to adjust learning rate. like RMSProp,Adadelta,Adam...and so on.they are time dependent.
* About loss function,cross entropy is for classification and MSE is for regression.
* When the value of cross entropy more small,the more similar between predict value and true value.
* MSE is very common use and is clearly to understand.
* Dropout is a way to avoid overfitting, the concept is useing less NN to train and enforce each NN to work more effective.
* Dropout is only for trainning stage,when in testing stage all of the NN are work.
* Dropout can decrease the loss and have well performance.
* There many activation function.but sigmoid and ReLU is most common to use.

# 2021-05-06#

* CNN is a common method in deep learning. Comparing to fully connected network, it's a better way to experss it.
* CNN do not show the weight that value equal to zero.
* Teacheer and student strategy is a good comcept. like teacher will teach something important and then student start to learn. 
* The Attention transfer's main concept is to distinguish the most important part.So that it can learn more efficiency.
* GAN need the punishment item to get generlizing and improve the accuracy.
* GAN has two NN,one is genarator and the other is discrimanate,thay training together. And propose is to let genarator NN create the image which most close to real one.

# 2021-05-13#

* Using Cross-validation (CV),Every sample of the data is used for both for training and testing in different folds.
* Underfitting and overfitting are two main problem to avoid.
* To watch the error diagram, we can Stop training when the validation error starts increasing. 
* Bias and Variance are two main concpet(like statistic).They all cause error increasing so need to meet the balance.
* Regularization is one of way to overcome overfitting, it use L1 or L2 term to avoid the model too similiar to dataset.
* L1 term = abs(a1+a2+...+an) ; L2 term = a1^2+a2^2+...+an^2 

# 2021-05-20#

* If the people who has bias,the AI system he write will seems to have bias to recognice.
* For example,when i use faceID,i can't unlock it when i wear without glasses.
* The video about fake person is really shocked for me, i am surprise such a high tech to achieve the imitatation of president Obama.
* I think we need to make a law to prevent someone use AI tech to hurt people.

# 2021-05-27#

* No class this week.
* I discuss final project with classmate, we tend to change the epoch and dropout rate to improve the accuracy.
* After some test, the best dropout rate is 0.5. if use 0.4 or 0.6,it still have worst proformance.
* We try to increase the epoch, it seems to have better accuracy, but might spend more time.

# 2021-06-03#

* Today's topic is about automatic and industril revolution.
* The 4th revolution is infer to the AI on everything,called AIOT.
* The revolution is start,althoughmany jobs will disappear at same time,the new kind of job will appear instead.
* Even in USA,such a strong country,the manufactor and agriculture is decrease, but another kind of economy,like finance or lawer...,is increasing.
* I discuss with my partner about group project.We try to input the data by our own file,but we failed,it can't run.So we just loaded on website.
* Just type " (x_train, y_train), (x_test, y_test) = mnist.load_data() "
* We learn use the Flatten is also a good way ,the concept is like to CNN, just doesn't have fulled connected.

# 2021-06-10#

* Review the handout about the lecture.
* If set larger batch size, it can get more accuracy learning. Otherwise, if set smaller, it can speed up learning.
* ELU function is also a activation function,the curve of it is seems like exponential. 
* The diffential the ELU function, we can have the linear curve. it will help to deal with deep learning.
* ELU can avoid vanishing gradient.
* Hyperparameters are chosen before the model is trained.

# 2021-06-17#

* About optimizer, I learn RMSprop is a update adjusts the Adagrad method in a very simple way in an attempt to reduce its aggressive, monotonically decreasing learning rate.
* We can also choose SGD,but it is slower than RMSprop.
* np.random.seed() : is a command to generate the same number.

# 2021-06-24#

* Review the DL course
* The main concept of ML is classification and regression.
* The type I error is status about False positive, it a wrong prediction.
* Learning rule is about many algorithm.
* Backpropagatioin is calculate how each weight contribute to the loss function, but we need to avoid vanishing gradient.
* optimization is conclude SGD、RMSProp、Adam.
* The larger epoch we set isn't lead to lower error, because overfitting. so we need to use early stop method to maintain lower error.
<<<<<<< HEAD
* about group project.
* original model we still have 98.9%, and the strategy to change Initializer, dropout and increase epochs.
* Finally we have 99.22% accuracy.
* By using dropout can prevent the overfitting in the model
* Higher epoch will give higher accuracy, but we should know when we have to stop the iteration to prevent overfitting in the model 


=======
* About the group project.
* original model have 98.9% accuracy, and the strategy to increase is conv2D,dropout and epoch.after it ,we get 99.22% accuracy.
* By using dropout can prevent the overfitting in the model
* Higher epoch will give higher accuracy, but we should know when we have to stop the iteration to prevent overfitting in the model 
>>>>>>> origin/Pao-Tsung-Chang/paotsung_changmd-edited-online-with-bitb-1624500841027


