This diary is written by Sam Sun for the course of Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.
# 2021.03.04 #
* The lecture gave me an overview about AI, MS, data mining, and deep learning.
* Data mining is crucial in future industries since people are generating and recording a big amount of data nowadays. If we can obtain patterns from these datas, we can invent new technologues and new product that fit people's need. 
* AI is to use computers to mimic human intelligence in some aspects. 
* ML can be considered as a subset of AI, aimimg to use computers to figure out informations from a set of data. 
* DL can be considered as a subset of ML, aiming to use the same techniques as ML to solve more complex problems. 

# 2021.03.11 #
* The lecture gave me a simple guide to use jupyter notebook and python. 
* From this lecture, I've learned how to use print function, and know how to find the syntax of certain function by help. 
* Using "/n" in print function can break the words into another line. 
* Actually, we're going to use tensorflow to build and train our model, so I've searched for some useful syntax websites about python and tensorflow: https://www.tensorflow.org/api_docs/python/tf
* It seems that the function of tensorflow are operated with specific object types, not like python that anything can be anytype of objects. I think this requires more practice and study to get used to it. 

# 2021.03.18 #
* We'd installed Anaconda, tensorflow, numpy locally in our computer in today's lecture. 
* WE'd learned some basic syntax of python in this lecture. 
* I've been programming in C# (for unity game developing) for the past year, and python is really different from C#. You don't need to identify the type of variables, which is not allowed in C#. Also, ";" is not needed for closing a sentence, the bracelets are not needed to claim a function or loop. Instead, a simple tab is needed to clain a function or loop. 
* For me, it's a little bit difficult to get used to the syntax of python, but I'll try to. 

# 2021.03.25 #
* The lecture today continues the syntax and function introduction of python as last week. 
* We'd learned how to create, add, and read a .txt file by means of python language. 
* Our suggestion is to use kahoot, debugging competition, or project realization to make the syntax course much interesting. 
* The other part is the introduction of digital image recognition realization. 
* By designing our own algorhythm, we can get input raw data as first layer, and using them to get numerous hidden layer, and finally make the decision. 
* Every layer consists of numerous neuron, every neuron holds a unique number that is determined by the information from the previous layer. 
* To ensure the importance and effectiveness of every neuron, we can transform the number held by every neurons into the range of [0,1], that is, using sigmoid function. 
* As for how to determine the amount of neurons and layers in the hidden layer, it depends on the resolution of result you desired, and the algorhythm you designed. 
* The class is getting more interesting now, I'm looking forward to the lectures of the future!

# 2021.04.01 #
* Today is holiday, there's no class, so my diary will be about the stuffs I'm doing in my leisure time. 
* I've attend an online class called "ML online learning for 100 days", I'm now studying the content inside, for basic python. 
* I'm gradualy getting used to python language now. Hope that this will help my final project. 

# 2021.04.08 #
* Today we watched a video that exactly demonstrate how to use tensor flow to realize hand-writing numbers recognization. 
* The lecture is a little bit hard for me, there's too much new function to learn. But I've re-watched the video again, and tried to get used to those essential functions. 
* I also studied my online course "ML online learning for 100 days", and built a basic knowledge about machine learning. 
* I'd also found some videos that I think they're the styles I would be easy to learn tensorflow. 
* Since my english is not so well, so I found some Chinese websites that explain neural network fine to me: (list below)
* https://zh.wikipedia.org/wiki/%E4%BA%BA%E5%B7%A5%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C
* https://www.ycc.idv.tw/ml-course-techniques_6.html

# 2021.04.15 #
* Today we learned about some theorem and concepts associated with AI. 
* The first one is about "No Free Lunch Theorem". It means that there's no best algorythm exist for all possibility of problems. 
* Despite that, we can still try to find best algorythm for "certain tasks". 
* We also talked about some deficiencies about AI, including the change of objectives. AI won't change their objectives due to their thoughts or feelings, but people will. 
* Another deficiency of AI is the simulation of the real world. People change their objectives and strategies due to a lot of real world factors, but we cannot appropriately simuate that kind of situation while developing or training AI. 
* I think the class today is a little bit boring. Hope that we can soon get into the practice and realization of AI. 

# 2021.04.22 #
* Today's lecture is about "Backpropagation". We watched several videos about how backpropagation really works on the class. 
* My english is not very good, so I cannot quite get to the point of the videos played on the class. As for as I can know, backpropagation is a method for us to realize or calculate "gradient descence", which is a way to train our AI. 
* Therefore, I went on the internet to search for materials that might help me. Eventually, I found this article useful: https://medium.com/%E4%B8%89%E5%8D%81%E4%B8%8D%E5%93%AD/%E6%A9%9F%E5%99%A8%E5%AD%B8%E7%BF%92%E8%87%AA%E5%AD%B8%E7%AD%86%E8%A8%9808-backpropagation-94e66e40a09c.
* Here comes my thought about gradient descent and backpropagation after reading several materials:
* 1. How do we estimate the "accuracy" of our AI? A viable way is to calculate the differences between our AI's answers and real answers, and then sum them up to estimate how far away our AI is compared with real answers. THe calculation of this difference is called "Loss Function".
* So, our problem become: how to reach the minimum of loss function, which represents that our AI is close to real answer? A viable way is to use "gradient descent". 
* Gradient descent, in short, is to calculate the slope of loss function, in order to adjust our parameters to find the combination that leads to the smallest loss function. 
* So how do we realize gradient descent? Backpropagation. 
* Backpropagation utilizes chain rules to calculate loss function. It's kinda like building a reverse AI of our current AI to train it. 
* As for the theories and calculation method, see in this papagraph:https://medium.com/%E4%B8%89%E5%8D%81%E4%B8%8D%E5%93%AD/%E6%A9%9F%E5%99%A8%E5%AD%B8%E7%BF%92%E8%87%AA%E5%AD%B8%E7%AD%86%E8%A8%9808-backpropagation-94e66e40a09c
* I'm happy that I finally understand what is backpropagation, and how to realizes it. 
* Another part of the class is about the Optimization methods. We didn't talked about this very much, and the most impressive one is the "Stochastic gradient descent".
* The SGD method is characterized by "Randomed-approaching trait" while optimizing our AI. The reason is that while every time we try to iterate our AI, the training dataset is different from last time's. So the loss function is not always the same in every training cases. 
* I think this course is very cool, however it's my problem that I cannot understand immediately while the videos are all english. 

# 2021.04.29 #
* Today, we introduced some different types of loss functions, such as Huber loss, cross entropy, Kullback-Leibler divergence......and so on. 
* Next, we talked about NN Regularization. A classical way to conduct regularization is to use "Drop Out" techniques. 
* So, what is "Drop Out"?
* While training models, we can randomly "dysfunction" some neurons, then train it. This forces every neuron to work hard to make right predictions, and prevent some dominant neurons to cover the results everytime, which might cause overfitting. 
* We can imagine that as training various "sub-models" of our models, and finally get a combined, well-functioned models. 
* The next thing is about drop out rate. How many neurons should I drop out during every training? Here's a website that gives some advice: https://machinelearningmastery.com/dropout-for-regularizing-deep-neural-networks/
* Some important things about drop out: 1. All the neurons are not "deleted", they are just randomly dysfunctioned during training. 2. The "dysfunction" only works during every forward and backward training process. 
* WHat about other benefits of drop out techniques? I'm still finding related paragraphs. Below are some of the articles them I found useful: 
* https://machinelearningmastery.com/dropout-for-regularizing-deep-neural-networks/
* https://towardsdatascience.com/regularization-in-deep-learning-l1-l2-and-dropout-377e75acc036
* https://www.analyticsvidhya.com/blog/2018/04/fundamentals-deep-learning-regularization-techniques/

# 2021.05.06 #
* In today's lecture, we talked about lots of techniques being used while actually training a model. 
* However, I didn't get the points of these techniques at all, I think the explanation and the graph used to explain is not so clear. 
* The only one that I thought that I can kind of understand it is the convolution calculation. It utilize specific filters, namely matrix, to get different features of the input image data, which can help the model to make classification much more precisely.
* As for deeper understand about CNN and how to realize CNN in python, I've found some websites that explains well for me: (list below)
* https://medium.com/jameslearningnote/%E8%B3%87%E6%96%99%E5%88%86%E6%9E%90-%E6%A9%9F%E5%99%A8%E5%AD%B8%E7%BF%92-%E7%AC%AC5-1%E8%AC%9B-%E5%8D%B7%E7%A9%8D%E7%A5%9E%E7%B6%93%E7%B6%B2%E7%B5%A1%E4%BB%8B%E7%B4%B9-convolutional-neural-network-4f8249d65d4f
* https://www.tensorflow.org/tutorials/images/cnn
* https://towardsdatascience.com/speeding-up-convolutional-neural-networks-240beac5e30f
* After understanding the contents of the websites above, I made some points about CNN: 
* 1.It uses a lot of filters to convolute with input image, to get many features of the original input. 
* 2.Using these features, our neural network can recognize the input images well.
* 3.As for in python and tensorflow, we can use Conv2D function to realize CNN. 
* 4.However, there are still some problems about CNN, including hard to train while layers are too much, long computation time, vanishing gradients......and so on. 
* Several method to reduce these defects have been proposed, including dropout and batch normalization. 
* Also, during realization of CNN by myself, I found some errors that might let the code dysfunction, the most common one is the input and output dimensions. The ways to solve them are listed below: 
* https://stackoverflow.com/questions/47665391/keras-valueerror-input-0-is-incompatible-with-layer-conv2d-1-expected-ndim-4

# 2021.05.13 #
* This week, we talked about the tasks needed to accomplish before final exam. I got a teammate, but she didn't come to class, and I don't know how to contact her. So sad. 
* Next week is our department's midterm exam, which will take five days to finish. So I didn't try much on building my final project, but I studied some different sorts of neural network. 
* This week, I learned a new learning structure: Recurrent neural network (RNN). To be short, RNN deals with time datas, which can affect each others even the time passed. Below are article explaining RNN: 
* https://ithelp.ithome.com.tw/articles/10188746
* RNN can also be realized on tensorflow, using RNN function. However, RNN has a serious problem: since every input can affect all the previous and future datas, the canculation amount is too big that it after happens gradient vanishing or exploding. To solve this issue, LSTM is introduced. 
* LSTM is like a modification of RNN that significantly avoid the problem of RNN. LSTM can also be realized on tensorflow, using LSTM function. 
* Is it possible that classification of MNIST datasets being realized by RNN? The answer is yes. To do so, just set every column of the input data as time series input for RNN. 
* I would like to try that on my funal project if the time is enough. 

# 2021.05.20 #
* This week is the first week of online course. Interesting experience using whereby. 
* We looked some videos together, including algorithmatic bias caused by human, how to prevent bias, and the techniques about making fake videos by pictures. 
* Teacher randomly seperated us to different small groups after we watched each video, and gave us some questions to discuss with each other. 
* The most interesting one is about algorithmatic bias and sharing experiences about it. I 'd worked on a project that uses patients' eye pressure data to predict if the patients have a kind of disease called glaucoma. However, we found out that our algorithm misjudge patients of Taiwanese much more rather than non-Taiwanese. Eventually, we developed tmo different algorithms for Taiwanese and non-Taiwanese. 
* I also learned about ResNet model. It's kinda like CNN, but prevent some defects of CNN. When it comes to deep learing using CNN, the calculation amount is so big that sometimes the training failed. ResNet utilizes skip connection to improve CNN. The input of each layers are as usual, but after every fixed layers, the input will be the past layer's output and output from fixed layers before. 
* ResNet sounds weird, but I found some websites that describes well: (list below)
* https://medium.com/%E8%BB%9F%E9%AB%94%E4%B9%8B%E5%BF%83/deep-learning-residual-leaning-%E8%AA%8D%E8%AD%98resnet%E8%88%87%E4%BB%96%E7%9A%84%E5%86%A0%E5%90%8D%E5%BE%8C%E7%B9%BC%E8%80%85resnext-resnest-6bedf9389ce
* https://www.itread01.com/content/1550581050.html
* https://towardsdatascience.com/creating-deeper-bottleneck-resnet-from-scratch-using-tensorflow-93e11ff7eb02

# 2021.05.27 #
* This week, I finally met my partner, and we tried to optimize our group project. 
* First, I use traditional model, which is flatten and activation. I changed epoch amounts, layers amount, layers output, activation fuction to see how the results changed. The results are around 97-98%, differences are not too large. Within 10 epoches, some models have increasing accuracy followed by decreasing accuracy, and the test result seem s to be under-expected. I think it might means overfitting. 
* I tried dropout methods, and the accuracy do imporved. But I'm not stisfied. 
* Therefore, I changed my model structure using CNN. It takes a much more time to calculate, but the results are stable and good, around 97%. 
* What's more, I tried some of the methods proposed on the articles to speed up ny CNN, including bottleneck structure and basis feature map. After my modification, I found a structure that can reach 98.9-99.2% accuracy after 10 epoches of training. I saved the model and tried to evaluate it, the results is stable. 
* Besides, I also tried RNN model, and I got 97-98% accuracy, too. At this point, I came up with an idea: how about combining CNN and RNN together? Unfortunately, the result is bad. "Don't use unecessary complicated structure to approach a goal that can be done by a much simple model".
* I also realized a simple version of ResNet model by myself, Actually the training results are not bad, around 95-96% accuracy, but I don't think is performs better than my modified CNN model. 
* I'll be keeping on trying to modify my current version of model. Maybe someday I will came up with some crazy ideas. 
* I also recorded my training process on my notion. I'll show them in my final project video. 

# Article summary #
* CNN is widely used on image classification, however the main defect of it is the long time consumption while training. 
* Several methods have been proposed, and this paper focus on using low ranks filter and basic feature maps to exploit the redundancy of calculation in traditional CNN. 
* They constructed two different schemes to realize their thoughts, and demonstrated the results. 
* The results showed that their method can speed up the process of CNN with 2.5x speed faster without decrease in accuracy, and 4.5x speed faster with less than 1% reduce in accuracy.
* Future works will be on exploring different accelerating method by using their techniques on basis. 

# 2021.06.03 #
* This week we talked about future jobs and how will AI affect and replace our future jobs. My feeling is that it's important for us to learn not only one skills for ourjobs, that will be dangerous if the job is replaced by AI, then we will have no job to do. 
* Another point for this week is the final project. This week I've came up with a new model structure, I call it "Squeeze Model". The origin of the model comes from this: I was originally using numerous convolutional layers, and the calculation takes too much time. Therefore, I tried to reduce this by some method, and a thought come up: If I want to get a convolution output with 32 filters, then how about I do this by two steps: first, do a convolution with 16 filters, then use the linear combination of these 16 feature maps to create 32 feature maps?
* T tried to realize this method, and I found the calculation time of each epoch reduced significantly from 2 minutes to nearly 30 seconds! How about the accuracy? There's no accuracy dropped compared with my original CNN!
* The best result I can get now is 99.22% by my squeeze model. But I'm still not satisfied, and I'm now trying to increase that by combination of my pretrained models. That is, pretrain 5 models with 99% accuracy, and then average their outputs. The best I can get now is 99.42%, and I believe I can reach more! Looking forward to future training and results. 

# 2021.06.17 #
* This week I combined my previous models into one model, and average their predictions to get new prediction. The best answer I can get is 99.60%. I think that's very good for me. 
* I've came up with two new ideas: DISCUSS_Model and SECONPREDIC_Moel, and I'm still trying to train them. If I got a beeter result, I'll present them in the class this week. 