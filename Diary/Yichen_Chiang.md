This diary file is written by Yichen_chiang T86081073 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2021-03-04 #

* The first lecture was easy to know about AI and DL.
* I take this course at the second week,it may takes me some times to know about the guide.
* Read about GIT introduction,and some commands how to use.
* Read "Elements of AI_Introduction to AI_Chapter 1:What is AI?"

# 2021-03-11 #

* Introduce how to use the Jupyter Notebook and the commands that we must to know about.
* Learn the code in Google Colaboratory，and also learn the magic in Jupyter's magics page.
* Get some concepts about IPython and Cython from the Jupyter's magics page.
* Read and practice the commands like "mnist.load_data()","Sequential()","model.compile()"in Nordling Lab - Python and Deep learning tutorial using TensorFlow 2.0 in Colaboratory.
* Prepare the successful stories PPT presentation.

# 2021-03-18 #

* practice tutorial_python_deep learning commands about Conditional statements and loops,range,for,Variables and functions,Classes and inheritance.
* write homework about making function and making a dictionary.
* read about python tutorial and make python commands advanced.

# 2021-03-25 #

* Practice tutorial_python_deep learning commands about reading and writing files,error handling.
* Learn the Neural Networks basic like sigmoid,hidden layer,CNN and three basic learning types.
* Student presentation about success stories show me various of views. 
* Think about how to make the basic python commands be more fun.

# 2021-04-08 #

* Watch the youtube video about "TensorFlow and Deep Learning without a PhD" by Martin Görner and ask some questions.
* Learn Ch. 5.1-5.3 in Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning.
* Read the write-up tutorial，it took me some time to understand what he done.
* Practice part of the code in Colab from Martin Görner's video.

# 2021-04-15 #

* Learn the concept about the neural network and different learning type.
* Discuss and share about the main idea to the no free lunch theorem.
* Discuss with classmate who sits next to me about what objective that we want to make.

# 2021-04-22 #

* Teach the function of neural network,limitations of current ANNs,machine learning basics about the no free lunch theorem,learning rules and backpropagation.
* Try to discuss with classmate about how can we change the objective orientation and what's the concept about the no free lunch theorem.
* Watch video about how the neural network can be done.
* Some classmates share the youtube video about neural network they found.

# 2021-04-29 #

* Learn the optimization methods(SGD,Adam,etc.),the learning rate,loss function.
* Try to discuss with classmate about what's advantage about dropout.
* Ask to find the youtube video about dropout can be done,when to dropout.

# 2021-05-06 #

* Learn the max pooling layer,one hot label,GAN,attention is all you need.
* Try to discuss with classmates about using CNN,rather than using full connected.
* Try to understand how is GAN (Generative Adversarial Network) working with discriminator and generator.

# 2021-05-13 #
* Learn the training and testing,overfitting,hyperparameter again,etc.
* Get some point about FFANEprot of SELU(activation function) and firefly algorithm.
* Watch TED's Talk about Ethics and the danger of algorithm bias,it's very insipiring me.
* Search on scientific article ontraining of Deep Neural Networks using supervised learning. 

# 2021-05-20 #
* Watch TED's talk about "How we can build AI to help humans, not hurt us","How I'm fighting bias in algorithms",and "Fake videos of real people -- and how to spot them" in class.
* Discuss these three videoes and share the thoughts about some questions of algorithms bias and fake video.
* After watching youtube videoes,write homework about stop button.

# 2021-05-27 #
* No class for this day.
* On May 27th,my project member and I discuss about how to train better model,how to improve,and also show the code that he wrote about to me.

# SeSe-Net: Self-Supervised deep learning for segmentation_summary #
* A novel self-supervised learning strategy is proposed to improve semantic segmentation models by taking advantage of unlabeled data. 
* It consists of “Worker”neural network (W) to segment given images and “Supervisor”neural network (S) to quantify the quality of the segmentation masks. 
* W can be first trained by using labeled data and generate masks for S to learn on. After that, W can be further trained and improved by using unlabeled data under S as self-supervised learning. 
* Our experimental results on three different datasets have demonstrated the significant improvement of SeSe-Net . 
* With an additional unlabeled dataset, SeSe-Net performs much better than challenging baseline methods. Furthermore, with less than 5% well-annotated training images, SeSe-Net performs on par with baseline methods. 

* I made PPT of this paper,and also made a prerecorded YouTube video. The YouTube video link: https://youtu.be/gPYLzWFBGBU

# 2021-06-03 #
* Today's course talk about our dream career and what type of job do we want to do?
* My dream job is to get into the pharmaceutical factory to be a analyst that deal with the data about clinical trial,and I want to get a long time job.
* Know about the nine planetary boundaries,self-driving electric vehicles,job automation that may happen in the decades.

# 2021-06-10 #
* Display the youtube videoes about the scientific article inspire me that AI is dynamic and multiple.
* My youtube video should get closer to the main point to present.
* Some classmates display their paper in the live broadcast, it takes very courage to complete.

# 2021-06-17 #
* No class for this day.
* Put the group project on group project files through github.
* My teammate and I discuss how to assign work to each other then complete PPT and the youtube video about group project.

# 2021-06-24 #
* Final class representation of group project is cool.
* Learn from group5's approach of LesNet a lot,made me think about the improve of the identification on Mnist.
* In conclusion,this class focus on interaction and thought that made me feel interesting and get deeper in AI field.
