 ## 2020/03/22 AI DIARY ENTRY
 
 
 ** This introduction to AI has been a revolution to me . I thought of artificial intelligence as just programming a mchine
 ** to do complicated tasks on it's own .I now realize that those machines can now have the power to think for itself. I
 ** this is a huge technological leap for a machine to make it's own decision. 
 
 
 ##        SUCCESS STORIES 
 
 **  A. I came across examples where a computer was trained to play chess , checkers and other games ;and was able to 
 ** comprehensively defeat it's human apponents. There is also a progam called alphago which is currently being used to
 ** to play a chinese board game. It is hoped that the software will be able to outperform a human expert.it is said that 
 ** the game has more possibilities than there are atoms in the universe , that means a lot of algorithms for the oomputer
 ** to learn . The interesting thing is that the software is not programmed to play the game but it is taught how 30,000
 ** amateurs play the game and from that it is able to learn to play on it's own . The software is then able to improve 
 ** playing itself after 3,000,000 times thereby defearing it's older version . After which comes an improved version.
 ** This is incredible since no more code has to be written ,instead the computer corrrects previous errors and make 
 ** much better decisons in the future.
 
 
 ** B.  AI has many uses in the classroom . In some instances it is able cross new frontiers in education. one example 
 ** it is able to be at the disposal of the child/children/learner at any given time . The child only has access to the
 ** during instructional time. Also , the technology helps each learner develop at his or her own pace. The learner is never
 ** under any stress to keep up with the rest of the class if he or she is a slow learner .
 
 
   ## 2020/ 03/26 WHAT IS AI 
   
   ** When it comes to AI there has no clear definition as to what it really is .What I gathered is that AI seems 
   ** to never have an  end because of the joke used to define AI. That is " the things computers cannot do" .
   ** it is a joke to the AI commununty this "joke" has a lot of merrit to it . This means the new frontiers will 
   ** be crossed as computers are able to achieve a lot more . 
   
   
   **A. AI  is not a countable noun and so adding to s at the end is not acceptable since AI is not an indivudual 
   ** but a complex system. For that aguement we treat AI as a collective noun and use terms like " a pinch of AI"
   ** and not "an AI " 
   
   **B. AI terms can be misleading , for example saying that thsi piece of software or robbot is intelligent would
   ** imply that this piece of technology can do things just like humans or have the ability to think critically 
   ** in many different situations. With AI that may not be the case whereas a computer can predict the temperature 
   ** with better accuracy than humans there are sveral basic functions human can perform that this software just 
   cannot achieve. 
   
   **C. What we humans consider as difficult seems to be a walk in the park for AI and what we consider easy tends 
   ** to be stumbling block to AI. Take the example of playing chess , checkers  or any game that requires calculations
   ** of millions or billions of algorithms in a short space of time , the computer excels at that .We humans would
   ** have nightmares even thinking of attempting such. The computer is so efficient that it can easily defeat the 
   **world champion in chess. On the other hand physical activities which we humans take for granted or consider easy 
   ** proves to be a herculean task for the computer because information gathered from many differnt sensory equipment
   ** must be gathered ,intepr eted and then make a decision about the outside world . 
   
   
   **D Some uses of AI include self driving cars and, image and video processing.
   ** in terms of self driving cars it is expected that road safety will be increased dramatically while the 
   efficiency of moving frieght will be unprecedented. 
   
   ## 2020/04 /02
   ** spring break 
   ** I tried going over the course content 
   
   ## 2020 /04/09
   **  tried to learn a little about python and what it was 
   ** did not understand the task 
   ** had no clue what to due with the pltattform 
   ** was hopping the class would be physical so I could ask questions sonce I am just stsrting to learn 
   ## 2020/04 /16 
   ** again had issues with the coding 
   
   ## 2020/04 /23 
   **   same as before more on coding 
   
   ## 2020 /04/30 
   ** more on coding 
   
   ## 2020 /05 / 07 
   
   ** codidng 
   
   
   ## 2020/05/14 
   
   **  Ethics of AI 
   ** AI looks like a capitalist idea , just abot profit 
   ** the reality is that lots of jobs will be lost due to AI 
   ** the solution is to become conversant in AI so that one can compete 
   ** there are still occuptions AI cannot replace
   
   ## 2020 /05 /28
   
   **  a physical class 
   ** was very intersiting as we were to discuss our dream jobs 
   ** I woild love to have an online buisiness , such that my clients are all over the world 
   ** i want to own my own business because Ii want more financial and personal freedom 
   ** I saw that AI was actually able to help me in acheiving that goal
   ** teacher explained why it is still th third industrial revolution
   ** very soon the electric car will be more economically viable than their gasoline counterparts 
   
   ## 2020 /06/04
   
  ** another physical class 
  
  ** which was very intersting as we cntinued from the previos week 
  ** i discussed with a colleauge the impact AI would have on society 
  **  we saw that if not managed well that it can cause civil unrest .
  ** this is because very few people employing AI will have tons of money while other persons work in the service 
     industry will be suffering with low incomes .
     
   ** This can be avoided by enesuring that the profits made by those AI operated companies are redistributed to help
       booost the salaries of those with lower salaries .
       
  ## 2020/06/18 
   .  The use of neural networks to train robots is remarkable but the end results always seem to astonish the creators
     1. A robbot was asked to walk with the minimum number of legs possible , the robot simply flipped over and walked on
        it's joints 
 **   2.a mechanical arm was asked to pick up a block , this was simple since the arm had a lever that can open and close 
        easily. In a later experiment the lever malfunctioned and so it was not able to open thus making it impossible for
        the mechanical arm to pick up the block or so was thought . The arm had an outside of the box solution , where it
        slammed the armed on the side of the box thus forcing the lever open afgter which it would then pick up the lock
  **   3. robots were able to separate food from poison using lights , they were able to cooperate with each other , after 
        a reward system was introduced they learnt how to decieve each other to become victorious .
        
  **   slides done by Chad Gaillard https://docs.google.com/presentation/d/1jlT7ZIKLZXq1oQkUv_25itbiTa523Fq9L-B1me1VYTk/edit#slide=id.p1
       
 **  video done by Chad Gaillard E24067028 https://youtu.be/CpvvZ3ZOdXw
 
 **  Summary of [Lehman et al.2019 The Surprising Creativity of Digital Evolution : A Collection of Anecdotes from Evolutionary
     Computation And Artificial Life Research Communities.](https://arxiv.org/abs/1803.03453)