This diary file is written by Po Ki Cheng H14088058 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-29 #

* I had some troubles regarding to the course enrolment. Luckily, I can now finally solve it and add this course successfully.
* This week, I just focused on the course materials available on moodle and figured out how the bitbucket works.
* I also read the materials of AI success stories which are very inspiring.
* I realized AI is closed related to ou daily life but not just about robots and other high-tech stuff.
* AI's usage is very wide, affecting not only the IT industry but also our entertainment, such as NBA, Pop music, or even transportation like plane.
* I am excited to learn more about AI, DL, most importantly, introduction to Python.

# 2020-04-04 #
* I learnt about Tensorflow this week.
* I've done with readings of Python.
* I don't have any prior knowledge of it, so it seems so difficult for me.

# 2020-04-11 #
* I still tried to familiar myself with those tools, but I started understanding most part of it.
* I did not spend too much time on this course this week. Busy with other mid terms.

# 2020-04-18 #
* I learnt about the principles of AI very thoroughly this week.
* It helps a lot in understand AI program as well.
* I read extra article about the topic and I found it very interesting. I really want to dig deeper on this topic.

# 2020-04-25 #
* The focus of this week is convolution layer and Neural Network.
* I understood why Relu function is more commonly used than sigmoid now.
* The fact that a slight change of line code can result a huge difference surprised me.

# 2020-05-03 #

* I just noticed that there is synchronized lectures in recent weeks. I did not receive any email and neither did I check moodle that frequently, which is my bad.
* I had the very first synchronized lecture and I found it quite effective that we could directly ask the Prof questions and have more interaction.
* Since I missed some lectures, I could not catch up with part of the class. Thats too bad!
* But then I tried to read through all the reading materials available and understand more about it after class.
* I definitely need to put more effort on catching up with the progress. 
* It is quite a pity that we could not have synchronized class earlier and everything was quite confusing. I wish I could start the course all over again.

# 2020-05-09 #
* Professor told us more about the group project and announced the group list.
* The task seems to be a bit difficult but I think we can nail it.
* The calculus part was a bit hard for me. But the basic terms of AI part was quite easy to follow.

# 2020-05-15 #
* The class has switched to offline lecture. 
* We had some discussion on ethics in AI, which inspired me a lot.
* With the advancement of AI, many functions can be achieved. But then does it mean we need to use AI to do it, even for some unethical purposes?
* I enjoyed the discussion with different people since I could gain more insights by listening to others' opinions.

# 2020-05-23 #
* I learnt more about automation this week and had a deep reflection on it.
* It is not only a technical problem, is about our society.
* Automation can replace some labor job, which of course can bring us convenience. But at the same time, people worried about the drop of job opportunity because of it.
* I believe that people would always improve ourselves and cater for the need of current market.
* There will be new jobs coming up in the future. It is another era of industry transformation.
* The conclusion is we, the future work force, need to not only strengthen our hrad skills. We also need to sharpen our soft skills which are applicable in any era and industries.
* Lifelong learning attitude, communication, teamwork skills cannot replaced by automation. 

# 2020-05-27 #
* Professor led us to think about our dream jobs in 10 years and introduced more about the industrial revolutions and the difference between them.
* This week was the first time for our group to meet up and discussed more on the group project.
* We divided the work and things went well. Hope this gorup project can be done smoothly and have good teamwork with my gorupmates.

# 2020-06-07 #
* The progress of the group project is quite good. Our group has finished the program and tested it for a few times.
* Our team also met up and talked about how to improve the program and did some amendment in this week.

# 2020-06-14 #
* The group project went so well. 
* Our group managed to finish it in this week and recorded the audio narration for the video presentation.
* Thank you to my groupmates' effort. The group project has pretty much summed up what we learnt from the course.
* Very good opportunity to put our knowledge into practice.

# 2020-06-21 #
* We submitted the group project in this week.
* I mainly focused on revision on the upcoming final exam.
* Wish it would go well. Good luck, everyone.

