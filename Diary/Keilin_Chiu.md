This diary file is written by Keilin Chiu F74065050 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-18 #

* I wrote my success story on how AI is used in wildlife conservation. 
* I found it very interesting on how AI can easily outperform a human in so many situations.
* By reading other peoples success story ppts, I see how AI can save time and human resources, by doing jobs more effectively.
* I feel inspired now to learn about AI and machine learning in hopes to impliment it in ways that can help people in the future. 

# 2020-03-25 #

* AI is being used in so many aspects of our daily lives, like self driving cars and image processing
* AI is complex but not as hard as it seems.
* AI has a lot of subfields such as machine learning, which also has a subfield called deep learning.
* Robotics is almost an ultimate combination of AI fields. It is very hard too acheive a well functioning Robot.
* It is amazing how far AI has come if its intellegence is on par to a human's 
* AGI must be insanely hard to achieve since so much is needed for there to be any process at all, so for now narrow AI is more effective.

# 2020-04-1 #

* This week we're learning python (on Getting help and comments, Printing, Conditional statements and loops)
* I realized that there are so many ways to learn python, be it a book, video or course. All of them are very useful but I prefer videos where i can directly look up and learn the part I am struggling with.  
* Here are a few things I learnt:

```python
* Comments *
# comments start with a hash

* Printing *
#this is an example of printing a string.
s = "This is a String" #this is a string variable
print(s) 
#or
print("This directly prints this String")

* Conditional Statements and Loops *

#If...else: #checks the condition before executing the next instruction
#Example 1:
if b > a: #if the condition "b>a" then the next line will be executed(remember the indentation or there will be an error)
  print("b is greater than a")
elif a == b: # elif means "else if" this is normally used when you want different results depending on different conditions
  print("a and b are equal")
  
#while loop #checks the condition before executing the next instruction
#Example 1:
i = 1
while i < 6:
  print(i)
  if i == 3:
    break #used to jump out a loop, in this case it will break when the above condition is met. (here only 1 2 3 will be printed.)
  i += 1
  
#for loop:
#Example 1:
fruits = ["apple", "banana", "cherry"] #this is an array of strings
for x in fruits: #for each in fruits execute the instruction below
  print(x)
#Example 2:
for x in range(2, 30, 3): #range can be used for a numbers between the given x and y. Eg( range(6) is 1-6, range(2,30) is 2-30 and range(3,6,2) is 3-6 in increments of 2)
  print(x)

```

# 2020-04-8 #

* This week we're learning more python (Variables and functions, Reading and writing files, Error handling)
* I have coding experience since I am a computer science major so most of this is quite familiar to me.
* This week the most complicated part to me would probably be the error handling, because it is easily fogotten that these errors can occur.
* Here are a few things I learnt:

```python
* Variables and functions *

#A variable is a storage location in memory that holds a value and is identified by the variable name.input() assigns the user inputted value to the var.
  a = 4 #eg of int var
  str = "this is a String" #eg of string var

#In Python, you define a function by using the def command. 
 def add(x,y): #an addition fuction that adds the x and y value when called eg. add(2,3) will return 5 
   return x + y

* Reading and writing files *

  f = open("Demofile.txt", "w+")  #using "w" writes in an exisiting file, where "w+" creates a new file and allows you to write in it.
  f.write("This is some text we wrote in the file.")
  f.close() 
  
  f = open("Demofile.txt", "r") #using "r" makes the file readable.
  print(f.read())
  f.close() 
  
  f = open("Demofile.txt", "a") #using "a" when opening a file will append the following written lines to the exisiting file.

* Error handling *
Syntax errors are the most basic and common type of error and arise when the Python parser can�t read a line of code. These errors typically cause the program to crash.
  eg:SyntaxError: invalid syntax 

For logical errors, the program doesn't crash but it produces unpredictable results.
  eg:z=5+1/2 #would 5.5 instead of a desired 3.
  
Sometimes even if a statement is syntactically correct, an error may arise when executing it. These errors are referred to as exceptions.
  ZeroDivisionError: division by zero
  NameError: Calling an undefined variable/function
  TypeError: Using the wrong type of data, e.g. adding a str to int

```

# 2020-04-15 #

* The online class today was very introductory, but it's an interesting approach how the professor wanted to know how he can change to improve the way he can teach us. Hoping for clear lessons in future online classes.
* The artificial neural network video was very interesting, it shows how complicated, time consuming and trial and error based AI really is. It surprised me how much maths was involved.
* There was quite for the reading and i found a hard time understanding all of it.
* I learnt that there are so many different tasks that machine learning can solve. I find it amazing how people have come up with these well performing algorithms to solve their different tasks, it must've taken so much time re-adjusting the algorithm just to lower the error rate each time.
* With the thousands of datasets from the tests performed there is so much room for error that getting it to work well may just be the hardest part of machine learning.

# 2020-04-22 #

* The second online class was okay but i hope to see the professor teach us the reasoning behind hard to understand concepts that is in the reading assigned to us.
* I watched a bit of the "TensorFlow and Deep Learning without a PhD" by Martin Görner video
* I was a bit surprised visualizing the huge amount of samppling and calculations done just to recognize one digit.
* I think the video simplified the concept of a deep neural network very nicely.
* Did some practice on Colaboratory.
* The write up provided to us was quite interesting to how it all works, but I think it would be challenging to grasp for those with no background in coding or these types of concepts.

# 2020-04-29 #

* witched the "How Convolutional Neural Networks work" video, found the identifying patterns and layering method very interesting.
* the pooling method is very clever that we dont have to match the pattern so accurately for the program to identify written letters, they can be a bit skew or different as everyones hand writing is so different in their own way. It also makes larger images easier to work with.
* Normalization keeps the maths from breaking and tweaks the numbers so that there are no negatives.
*   - Convolution- filtering/matching pixels to a pattern, section by section, this becomes a layer.
       - Rectified Linear Units layer(ReLU) - stack of images with no negative values.  
       - Pooling - Shrinking the image stack by taking one number to represent the pixel window.
* Convolution leverages three important ideas that can help improve a machine learning system:sparse interactions,parameter sharing and equivariant Representations.

# 2020-05-6 #

* The videos were a bit complicated for me since I'm not familiar with the complex maths but I think it was explained well enough to understand the concept.
* It was nice to visualize gradient decent because its a bit overwhelming when we see the equation.
* propagating backwards seems like a very tedious process to increase the acuracy of the network.
* Stochastic gradient descent is the main way to train large linear models on very largedatasets,  one can argue that the asymptotic cost of traininga model with SGD is O(1) as a function of m.
* I likes how the "Using neural nets to recognize handwritten digits" article showed us the code it was quite interesting how a few lines of code could do so much.

# 2020-05-13 #

* I found all the videos very interesting, I didnt realize how biased programs can become, but it is logical how with a different test data set it might come to a completely different outcome
* The modelling of faces was very impressive.
* I really liked the stop button video because it shows how cleaver the machine really becomes and that simple actions like pressing a button, becomes quite hard to impliment.
* Machines can become smart to the point that it would act how you want in test conditions but after optimizing itself it could take out code that was implimented for safety reasons since it doesnt benefit the machine it might deem the code useless.

# 2020-05-20 #

* we had physical class this week, there was a lot of discussion 
* I wished the professor did more of a lecture to help us understand the concepts used in the videos he showed us
* I only found one of my groupmates we are going to start discussing and splitting our work this week 
* I did like the discussion with personal examples and ideas but I think it was too long, took up too much lesson time.

# 2020-05-27 #

* my groupmates all dropped the course and I'll have to do the project alone
* instructions for the project arent very clear i dont think this course is suitable for a GE class
* just started the project

# 2020-06-03 #

* discussions on how ai will take over a large percentage of human labor
* talks on how we must keep learning to keep up with the teachnology and not be replaced by them

# 2020-06-10 #

* Worked more on the project 
* thinking of ways to improve code

# 2020-06-17 #

* recorded the video for the project 
* reviewed material